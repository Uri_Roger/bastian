<?php

// REDEFINIMOS LA QUERY DE LA HOME, PARA QUE COJA BIEN TODOS LOS PRODUCTOS
/*
function bastian_views_query_alter(&$view, &$query) {
  if ($view->name == 'front_page_2') {

	if ($query->table_queue['field_data_field_tipo_de_recurso']) {
		$query->table_queue['field_data_field_tipo_de_recurso']['join']->type = 'LEFT';
	}

	if ($query->table_queue['field_data_field_tipo_de_producto']) {
		$query->table_queue['field_data_field_tipo_de_producto']['join']->type = 'LEFT';
	}
  }
}
*/




// HACEMOS "UNION" DE LA QUERY DE LA HOME, Y LA QUERY DE CONTENIDO TIPO "ESCAPARATE"
/*
function bastian_views_pre_execute(view &$view) {
  global $language;

  if ($view->name == 'front_page_2') {
    $query1 = &$view->build_info['query'];

    // Basic setup of the second query.
    $query2 = db_select('node', 'node')
      ->condition('type', 'escaparate', '=')
      ->condition('language', $language->language, '=')
      ->condition('status', 1, '=');

    $query2->addField('node', 'nid', 'nid');
    $query2->addField('node', 'sticky', 'sticky');

    $query1->addField('node', 'sticky', 'sticky');
	$query1->orderBy('sticky','DESC');
	$query1->range(0, 20);
//	dpm ($view);

    // Matrimony.
	$query1 = $query2->union($query1, 'UNION ALL');
  }
}

function bastian_views_pre_render(&$view) {
//  dpm($view);
  if ($view->name == 'front_page_2') {
	foreach ($view->result as $key => $value) {
		$node = node_load($value->nid);
		if ($node->type == "escaparate") {
			$pos = rand(0,8);
			$new = $view->result[$pos];
			$view->result[$pos] = $view->result[$key];
			$view->result[$key] = $new;
		}
	}
  }
}
 *
 */