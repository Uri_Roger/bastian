<?php
/**
 * @file
 * Views hooks for Ubercart product attributes.
 */

/**
 * Implements hook_views_data().
 */

function uc_attribute_views_data() {
  $data['uc_product_options']['table']['group'] = t('Product attributes');

  $data['uc_product_options']['table']['join'] = array(
    'node' => array(
      'table' => 'uc_product_options',
      'left_field' => 'nid',
      'field' => 'nid',
    ),
  );

  $result = db_query("SELECT aid, name, description FROM {uc_attributes}");
  foreach ($result as $row) {
    $data['uc_product_options']['attr_' . $row->aid] = array(
      'title' => 'Attribute: ' . $row->name,
      'help' => 'Attribute desc: ' . $row->description,
      'real field' => 'oid',
      'group' => t('Product'),
      'field' => array(
        'handler' => 'views_handler_field',
        'click sortable' => TRUE,
        'group' => t('Product'),
      ),
      'filter' => array(
        'handler' => 'uc_attribute_handler_filter_attribute',
        'label' => 'Attribute',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_string',
      ),
    );
  }

  return $data;
}



/*
function uc_attribute_views_data() {
  $data['uc_product_adjustments']['table']['group'] = t('Product');

  $data['uc_product_adjustments']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
  );

  $result = db_query("SELECT aid, name, description FROM {uc_attributes}");
  foreach ($result as $row) {
    $data['uc_product_adjustments']['attr_' . $row->aid] = array(
      'title' => 'Attribute: ' . $row->name,
      'help' => 'Attribute desc: ' . $row->description,
      'real field' => 'combination',
      'group' => t('Product'),
      'field' => array(
        'handler' => 'views_handler_field',
        'click sortable' => TRUE,
        'group' => t('Product'),
      ),
      'filter' => array(
        'handler' => 'uc_attribute_handler_filter_attribute',
        'label' => 'Attribute',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_string',
      ),
    );
  }

  return $data;
}
*/

/*
function uc_attribute_views_data() {
  $data = array();
  $uc_product_attributes = drupal_get_schema_unprocessed('uc_attribute', 'uc_product_attributes');

  //Create a field for each option value for each product, they could be null
  $result = db_query("SELECT * FROM {uc_attribute_options}");
  foreach($result as $row) {
    $attr = uc_attribute_load($row->aid);
    $option = uc_attribute_option_load($row->oid);
    $data['uc_product_options_' . $row->aid . '_' . $row->oid]['table']['group'] = t('Product attributes');
    $data['uc_product_options_' . $row->aid . '_' . $row->oid]['table']['join']['node'] = array(
        'table' => 'uc_product_options',
        'left_field' => 'nid',
        'field' => 'nid',
        'extra' => array(
          array(
            'field' => 'oid',
            'value' => $row->oid,
          ),
        ),
    );
    $data['uc_product_options_' . $row->aid . '_' . $row->oid]['table']['group'] = t('Product attributes');
    $data['uc_product_options_' . $row->aid . '_' . $row->oid]['price'] = array(
      'title' => t('Price for !aid/!oid', array('!aid' => $attr->label ? $attr->label : $attr->name, '!oid' => $option->name)),
      'help' => 'Price of the product with the selected option.',
      'field' => array(
        'handler' => 'uc_product_handler_field_price',
        'click sortable' => TRUE,
        'float' => TRUE,
      ),
    );
    $data['uc_product_options_' . $row->aid . '_' . $row->oid]['cost'] = array(
      'title' => t('Cost for !aid/!oid', array('!aid' => $attr->label ? $attr->label : $attr->name, '!oid' => $option->name)),
      'help' => 'Cost of the product with the selected option.',
      'field' => array(
        'handler' => 'uc_product_handler_field_price',
        'click sortable' => TRUE,
        'float' => TRUE,
      ),
    );
    $data['uc_product_options_' . $row->aid . '_' . $row->oid]['weight'] = array(
      'title' => t('Weight for !aid/!oid', array('!aid' => $attr->label ? $attr->label : $attr->name, '!oid' => $option->name)),
      'help' => 'Weight of the product with the selected option.',
      'field' => array(
        'additional fields' => array(
          'weight_units' => array(
            'table' => 'uc_products',
            'field' => 'weight_units',
          ),
        ),
        'handler' => 'uc_product_handler_field_weight',
        'click sortable' => TRUE,
        'float' => TRUE,
      ),
    );
  }

  return $data;
}
*/