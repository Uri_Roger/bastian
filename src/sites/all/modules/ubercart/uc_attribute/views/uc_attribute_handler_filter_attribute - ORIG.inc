<?php
/**
 * @file
 * Views handler: Node filter on "product-ness".
 */

/**
 * Filters nodes based on whether they have an entry in the uc_products table.
 */
class uc_attribute_handler_filter_attribute extends views_handler_filter_in_operator {

  /**
   * Overrides views_handler_filter_in_operator::get_value_options().
   */
  function get_value_options() {
    if (isset($this->value_options)) {
      return;
    }
    $this->value_options = array();
    $aid = explode('_', $this->field);
    $aid = $aid[1];
    $result = db_query("SELECT name, oid FROM {uc_attribute_options} WHERE aid = :aid ORDER BY ordering", array(':aid' => $aid));
    while ($row = $result->fetchObject()) {
      $this->value_options[$row->oid] = $row->name;
    }
  }

  /**
   * Overrides views_handler_field::query().
   */
  function query() {
    $this->ensure_my_table();
    dpm($this);
    $aid = explode('_', $this->field);
    $aid = $aid[1]; 
    dpm($aid);
    $field = "{$this->table_alias}.{$this->real_field}";
    foreach ($this->value as &$value) {
      // Match a part of serialized attributes string.
      $value = sprintf('%%i:%d;s:%d:"%d"%%', $aid, strlen($value), $value);
      $this->query->add_where($this->options['group'], $field, $value, $this->operator == 'not in' ? 'NOT LIKE' : 'LIKE');
      dpm($this->query);
    }
  }
}