<?php
/**
 * @file
 * Override of preprocess functions.
 */
function tb_wall_preprocess_html(&$vars) {
  $current_skin = theme_get_setting('skin');
  if (isset($_COOKIE['nucleus_skin'])) {
    $current_skin = $_COOKIE['nucleus_skin'];
  }
  $vars['nucleus_skin_classes'] = !empty($current_skin) ? ($current_skin . "-skin") : "";
  
  if(isset($vars['theme_hook_suggestions'][2]) && $vars['theme_hook_suggestions'][2] == 'html__node__250'){
    $vars['theme_hook_suggestions'][] = 'html__404';
  }
  elseif (count($vars['theme_hook_suggestions']) == 1) {
    if (isset($vars['page']['content']['system_main']['main']['#markup']) &&
            trim($vars['page']['content']['system_main']['main']['#markup']) == t('The requested page "@path" could not be found.', array('@path' => request_uri()))) {
      $vars['theme_hook_suggestions'][] = 'html__404';
    }
  }
  if (isset($_GET['tb_wall_iframe']) && $_GET['tb_wall_iframe'] == 1) {
    $vars['theme_hook_suggestions'][] = 'html__popup_iframe';
  }
}

function tb_wall_preprocess_page(&$vars) {
  drupal_add_js("
    Drupal.TBWall = Drupal.TBWall || {};
    Drupal.TBWall.lazyload_icon = '" . base_path() . drupal_get_path('theme', 'tb_wall') . "/images/loading-img.gif';
  ", 'inline');
  if (isset($_GET['tb_wall_iframe']) && $_GET['tb_wall_iframe'] == 1) {
    $vars['theme_hook_suggestions'][] = 'page__popup_iframe';
  }


//  dpm($vars['theme_hook_suggestions']);


  // ACORDE�N EN LOS FILTROS DE �MBITOS DE LA HOME 

	// Add ui accordion js.
	drupal_add_library('system', 'ui.accordion');


	drupal_add_js("	
		jQuery(document).ready(function () { 
			jQuery('#edit-ambito-wrapper .bef-tree-depth-0 li').not('#edit-ambito-wrapper .bef-tree-depth-1 li').each(function() {
				if (jQuery(this).children('ul').find('input:checkbox[checked=\'true\']').length > 0)
					jQuery(this).accordion({active:0},{collapsible: true});
				else		
					jQuery(this).accordion({active:false},{collapsible: true});
				
				jQuery(this).children('.form-item').filter(function () {
					var classes = jQuery(this).attr('class').split(' ');
					for (var i=0; i<classes.length; i++) {
						if (classes[i].slice(0,22) === 'form-item-edit-ambito-') {
							ambito = classes[i].slice(22,classes[i].length);
//							alert(ambito + ' - ' + ambito_taxonomy_name(ambito));
							ambitoName = ambito_taxonomy_name(ambito);
							jQuery(this).addClass('icono-ambito');
							jQuery(this).addClass('icono-ambito-' + ambitoName);
						}
					}
				});
				
				
			});

			jQuery('.bef-tree-depth-0 input[type=checkbox]').click(function(e) {
				e.stopPropagation();
			});
		}
	);", "inline");	



}

function tb_wall_preprocess_node(&$vars) {
  if($vars['type'] == 'tb_social_feed') {
    if(isset($vars['field_social_feed_type'])) {
  	  foreach ($vars['field_social_feed_type'] as $style) {
  	    if(isset($style['taxonomy_term'])) {
          $vars['classes_array'][] = drupal_strtolower($style['taxonomy_term']->name);
  	    }
  	    else {
  	      foreach($style as $term) {
  	      	if(isset($term['tid'])) {
              $t = taxonomy_term_load($term['tid']);
              $vars['classes_array'][] = drupal_strtolower($t->name);
            }
            else {
              $vars['classes_array'][] = drupal_strtolower($t->name);
            }  	        	
  	      }
  	    }
      }
    }  	
  }


  if ($vars['view_mode'] == 'full') {
		// TIPO DE CONTENIDO "PRODUCTO"
//		if($vars['type'] == 'product') {
			drupal_add_js("	
				jQuery(document).ready(function () { 

// 					RECOLOCAMOS CONTENIDO A IZQUIERDA Y DERECHA

//					jQuery('.node-full .header').insertAfter('.node-full .galleryformatter');
//					jQuery('.node-full .header').insertBefore('.node-full .node-content');
//					jQuery('.node-full .group-product-right').prepend(jQuery('.node-full .header'));
//					jQuery('.node-full .group-column-left').prepend(jQuery('.node-full .galleryformatter'));

//					jQuery('.node-full .galleryformatter').prependTo(jQuery('.node-full .group-superior .group-galleryformatter'));
//					jQuery('.node-full .group-superior .group-galleryformatter').prepend(jQuery('.node-full .galleryformatter'));

//					jQuery('<div class=\"group-galleryformatter\"></div>').insertBefore(jQuery('.node-full .field-name-field-aviso-club'));

//					jQuery('<div class=\"group-galleryformatter\"></div>').insertBefore(jQuery('.node-full .group-product-right'));
					jQuery('<div class=\"group-galleryformatter\"></div>').insertAfter(jQuery('.node-full .group-compartir'));
					jQuery('.node-full .group-galleryformatter').prepend(jQuery('.node-full .galleryformatter'));
//					jQuery('.node-full .group-superior').prepend(jQuery('.node-full .group-compartir'));

					
					 

//					jQuery('.node-full .galleryformatter').first().remove();
//					alert(jQuery('.node-full .galleryformatter').last().parent().attr('class'));


//					jQuery('.node-full .group-column-left').insertBefore('.node-full .node-content');
//					jQuery('.node-full .group-product-left').insertBefore('.node-full .node-content');
//					jQuery('.node-full .group-product-right').insertBefore('.node-full .node-content');


/*					jQuery('.node-full .field-name-field-tipo-de-contenido').insertBefore('.node-full .header');
					jQuery('.node-full .field-name-field-sello-calidad-auto').insertBefore('.node-full .node-content');
					jQuery('.node-full .flag-outer').insertBefore('.node-full .node-content');

					jQuery('.node-full .group-caracteristicas').insertBefore('.node-full .node-content');
//					jQuery('.node-full .field-name-field-ambito').insertBefore('.node-full .node-content');	
					jQuery('.node-full .field-name-field-aviso-club').insertBefore('.node-full .node-content');
					jQuery('.node-full .group-add-to-cart').insertBefore('.node-full .node-content');

					jQuery('.node-full .group-rating').insertBefore('.node-full .node-content');
					jQuery('.node-full .group-ambito').insertBefore('.node-full .node-content');
*/

// 					CAMBIAMOS TAMA�OS DE LAS IM�GENES DE TAXONOM�A (EDAD Y �MBITOS)

/*					var tax_image = jQuery('.taxonomy-term .field-type-image img');
					tax_image.each(function() {
						var image_data = jQuery(this).attr('data-src');

						var name = image_data.substr( 0, (image_data.lastIndexOf('.') ) ); 
						var extension = image_data.substr( (image_data.lastIndexOf('.') + 1) ); 
						var new_name = name + '_big.' + extension;
						jQuery(this).attr('data-src', new_name)
						jQuery(this).width(37).height(37);
					});
*/
				}
			);", "inline");	
//		}





  }
/*
  elseif ($vars['view_mode'] == 'teaser') {
		// TIPO DE CONTENIDO "PRODUCTO"
		if($vars['type'] == 'product') {
			// RECOLOCAMOS CONTENIDO A IZQUIERDA Y DERECHA
			drupal_add_js("	
				jQuery(document).ready(function () { 
					jQuery('.node-full .group-franja-superior').insertBefore('.node-teaser .node-content');
				}
			);", "inline");	
		}
  }
*/

/*
		drupal_add_js("	
			jQuery(document).ready(function () {
				contenido = jQuery('div.tipo-contenido div');
				tipo = contenido.attr('class');
				contenido.prepend('<img src=\"images/tipo-small-Libro.png\">') ;
			}
		);", "inline");	
*/

		// ALINEAMOS LAS CAPAS POR ARRIBA Y POR ABAJO

  if ($vars['view_mode'] == 'full') {

		drupal_add_js("	
			jQuery(document).ready(function () {
/*				if (jQuery('.group-superior').find('.group-ambito').length) {
					var div5 = jQuery('.group-ambito');
				}
				else
					var div5 = jQuery('.group-caracteristicas');



				var div5 = jQuery('.group-caracteristicas');	

				var div1_outerHeight = jQuery('.group-superior').outerHeight();
				var div2_outerHeight = jQuery('.group-product-left').outerHeight();
				var div3_outerHeight = jQuery('.group-superior .group-galleryformatter').outerHeight();
				var div5_outerHeight = div5.outerHeight();

				var div5_posicion = div5.position();
				var segundo = div5_posicion.top + div5_outerHeight;

				var relleno = Math.abs(div1_outerHeight - div2_outerHeight);

				relleno += 5;
	*/
//				alert(div2_outerHeight);
//				alert(div1_outerHeight + ' - ' + div5_outerHeight + ' - ' + div5_posicion.top + ' - ' + segundo + ' - ' + relleno);
//				div5.css('height', (div5_outerHeight + relleno) + 'px');





/*
				var div5 = jQuery('.group-caracteristicas');	
//				var div5 = jQuery('.group-product-right');	

				var div1_outerHeight = jQuery('.group-superior').outerHeight();
				var div2_outerHeight = jQuery('.group-product-right').outerHeight();
				var div3_outerHeight = jQuery('.group-superior .group-galleryformatter').outerHeight();
				var div5_outerHeight = div5.outerHeight();

				var div5_posicion = div5.position();
				var segundo = div5_posicion.top + div5_outerHeight;

//				var relleno = Math.abs(div1_outerHeight - segundo);
				var relleno = Math.abs(div1_outerHeight - div2_outerHeight);

				relleno += 5;

//				alert(div1_outerHeight + ' - ' + div5_outerHeight + ' - ' + div5_posicion.top + ' - ' + segundo + ' - ' + relleno);
				div5.css('height', (div5_outerHeight + relleno) + 'px');
*/
/*
//				var div2_outerHeight = jQuery('.group-superior .field-name-uc-product-image').outerHeight(true);
//				var div5_outerHeight = div5.outerHeight();
//				var div5_height = div5.height();
//				var div5_padding = div5_outerHeight - div5_height;

//				var div5_posicion = div5.position();
//				var segundo = div5_posicion.top + div5_outerHeight;

//				var relleno = Math.abs(div2_outerHeight - segundo);

				var relleno = Math.abs(div2_outerHeight - div3_outerHeight);

//				alert(div2_outerHeight + ' - ' + div1_outerHeight);

				if (div2_outerHeight > segundo) {
//					alert(div2_outerHeight + ' - ' + div5_height + ' - ' + div5_outerHeight + ' - ' + segundo + ' - ' + relleno);
//					div5.css('height', (div5_outerHeight + relleno) + 'px');
				}
				else {
//					jQuery('.field-name-uc-product-image').css('height', (div2_outerHeight + relleno) + 'px');
//					jQuery('.group-galleryformatter').css('height', (div2_outerHeight + relleno) + 'px');
				}

				if (div1_outerHeight != div2_outerHeight) {
//					div5.css('height', (div5_outerHeight + relleno) + 'px');
					div5.css('height', div2_outerHeight + 'px');
				}
				else {
					alert(div2_outerHeight + ' - ' + div1_outerHeight);
//					jQuery('.group-galleryformatter').css('height', (div3_outerHeight + relleno) + 'px');
					jQuery('.group-galleryformatter').css('height', div2_outerHeight + 'px');
				}
*/

/*				var div1_outerHeight = jQuery('.galleryformatter').outerHeight();
				var div5_outerHeight = div5.outerHeight();

				var div5_posicion = div5.position();
				var segundo = div5_posicion.top + div5_outerHeight;

				if (div1_outerHeight > segundo) {
					var relleno = div1_outerHeight - segundo;
					var total = div5_outerHeight + relleno;

//					alert(div1_outerHeight + ' - ' + div5_outerHeight + ' - ' + div5_posicion.top + ' - ' + segundo + ' - ' + relleno + ' - ' + total);

					div5.css('height', total + 'px');
				}
				else {
					var relleno = segundo - div1_outerHeight;
					var total = div1_outerHeight + relleno;

//					alert(div1_outerHeight + ' - ' + div5_outerHeight + ' - ' + div5_posicion.top + ' - ' + segundo + ' - ' + relleno + ' - ' + total);

					jQuery('.group-galleryformatter').css('height', total + 'px');
				}
*/




			}
		);", "inline");	

	}


  if($vars['view_mode'] == 'teaser') {
  
  	if(isset($vars['content']['links']['comment']['#links']['comment-add'])) {
  	  unset($vars['content']['links']['comment']['#links']['comment-add']);
  	}
	else if(isset($vars['content']['links']['comment']['#links']['comment_forbidden'])) {
	  unset($vars['content']['links']['comment']['#links']['comment_forbidden']);
	}
  	if($vars['node']->type == 'tb_rss_feed' || $vars['node']->type == 'tb_social_feed') {
  	  if(isset($vars['content']['links']['node']['#links']['node-readmore'])) {
  	    unset($vars['content']['links']['node']['#links']['node-readmore']);
  	  }
  	}
  }
  $vars['date'] = format_date($vars['node']->created, 'custom', 'd M Y');
  $vars['tb_wall_first_field'] = false;
  foreach($vars['content'] as $key => $field) {
    if (isset($field['#field_type']) 
      && isset($field['#weight']) 
      && ($field['#field_type'] == 'image' || $field['#field_name'] == 'field_vimeo' || $field['#field_name'] == 'field_youtube' || $field['#field_name'] == 'field_video' || $field['#field_name'] == 'field_media')) {
      $vars['tb_wall_first_field'] = drupal_render($field);
      unset($vars['content'][$key]);
      break;
    }
  }
  if(variable_get('clean_url', 0)) {
    $node_url = $vars['node_url'];
    $vars['tb_wall_iframe_token'] = (strpos($node_url, "?") !== false) ? "&" : "?";
  }
  
  // process theme style
  $skins = nucleus_get_predefined_param('skins', array('default' => t("Default skin")));
  foreach ($skins as $key => $val) {
    if ($vars['node_url'] == base_path() . 'skins/' . $key && (!isset($_COOKIE['nucleus_skin']) || $_COOKIE['nucleus_skin'] != $key)) {
      setcookie('nucleus_skin', $key, time() + 100000, base_path());
      header('Location: ' . $vars['node_url']);
    }
  }
}

/**
 * Implements hook_css_alter().
 */
function tb_wall_js_alter(&$js) {
  if (isset($js[drupal_get_path('module', 'views_infinite_scroll') . '/js/views_infinite_scroll.js']) &&
     (isset($js[drupal_get_path('module', 'views_infinite_scroll') . '/js/jquery.autopager-1.0.0.js']) || 
      isset($js[libraries_get_path('autopager') .'/jquery.autopager-1.0.0.js']))
  ) {
    drupal_add_js(drupal_get_path('theme', 'tb_wall') . '/js/jquery.autopager-1.0.0.js');
    drupal_add_js(drupal_get_path('theme', 'tb_wall') . '/js/views_infinite_scroll.js');
  }
  if (isset($js[drupal_get_path('module', 'colorbox') . '/js/colorbox_load.js'])) {
    drupal_add_js(drupal_get_path('theme', 'tb_wall') . '/js/colorbox_load.js');
  }
}

function tb_wall_teaser_class_fields() {
  return array(
    'field_social_feed_type' => array(
      'default' => false,
    ),
    'field_tb_wall_style' => array(
      'default' => 'tb-wall-single-style',
    ),
    'field_tb_wall_background' => array(
      'default' => false,
    ),
    'field_tb_wall_badge' => array(
      'default' => false,
    )
  );
}

function tb_wall_get_teaser_class($node) {
  $classes = array(); 
  $fields = tb_wall_teaser_class_fields();
  foreach($fields as $field => $info) {
  	$class = false;
    if(isset($node->$field)) {
      $field_content = $node->$field;
      foreach($field_content as $lang) {
        foreach($lang as $term) {
          if(isset($term['taxonomy_term'])) {
            $class = drupal_strtolower($term['taxonomy_term']->name);
          }
          else {
            $t = taxonomy_term_load($term['tid']);
            if($t) {
              $class = drupal_strtolower($t->name);
            }
          }
        }
      }
    }
    $class = $class ? $class : $info['default'];
    $existed = false;
    if($class) {
      $classes[] = $class;
    }
  }
  return $classes;
}

function tb_wall_preprocess_views_view_grid(&$vars) {
  $tb_wall_classes = array();
  $view = $vars['view'];
//  dpm($view);

  // SI ESTAMOS EN UNA P�GINA DE TAXONOM�A, SELECCIONAMOS EL T�RMINO ACTUAL EN LOS FILTROS DE LA VIEW
  if (arg(0) == 'taxonomy' && is_numeric(arg(2))) {
	$actual = arg(2);

	$view->exposed_raw_input["Ambito"][] = $actual;
	$input = "edit-ambito-" . $actual;

	drupal_add_js("	
		jQuery(document).ready(function () { 
			jQuery('.views-exposed-widgets #" . $input . "').attr('checked',true);
			jQuery('.views-exposed-widgets .form-item-Ambito fieldset').removeClass('collapsed');			
		}
	);", "inline");	

  }

  // SI NO ES USUARIO TIPO "PREMIUM", MOSTRAMOS DESACTIVADAS LAS OPCIONES DE �MBITO
  if (!bastian_is_premium()) {
	drupal_add_js("	
		jQuery(document).ready(function () { 
			jQuery('#edit-ambito-wrapper input').attr('disabled', 'disabled');
			jQuery('#edit-ambito-wrapper label').first().append(' (solo Premium)');
		}
	);", "inline");	
  }



  if (isset($view->style_plugin->row_plugin->nodes)) {
    $nodes = $view->style_plugin->row_plugin->nodes;
    $counter = 0;
    foreach($nodes as $node) {
      $row_extend_class = tb_wall_get_teaser_class($node);
      $tb_wall_classes[$counter] = implode(" ", $row_extend_class);
      $counter ++;
    }
  }
  $vars['tb_wall_classes'] = $tb_wall_classes;
}

function tb_wall_preprocess_field(&$vars) {
  if($vars['element']['#field_type'] == 'image' && $vars['element']['#entity_type'] == 'node' && $vars['element']['#view_mode'] == 'teaser') {
    foreach ($vars['items'] as $key => $item) {
      if ($item['#image_style'] == 'tb-wall-dynamic-style') {
        $img_style = false;
        if (isset($vars['element']['#object']->field_tb_wall_style)) {
          foreach ($vars['element']['#object']->field_tb_wall_style as $style) {
            foreach ($style as $lang) {
              foreach ($lang as $term) {
                $t = isset($term->name) ? $term : taxonomy_term_load($term);
                $image_style = image_style_load($t->name);
                if ($image_style) {
                  $img_style = $t->name;
                }
              }
            }
          }
        }
        if ($img_style) {
          $vars['items'][$key]['#image_style'] = $img_style; 
        }
        else {
          $image_style = image_style_load('tb-wall-single-style');
          if ($image_style) {
            $vars['items'][$key]['#image_style'] = 'tb-wall-single-style'; 
          }
        }
      }
    }
  }

  // ICONOS TIPO DE CONTENIDO  
  if($vars['element']['#field_name'] == 'field_tipo_de_contenido') {
//  	dpm($vars);
      $tipo = $vars['element']['#bundle'];
      $title = $vars['items']['0']['#markup'];
	  $vars['items']['0']['#markup'] = "<a class='tipo-" . $tipo . "' href='#' title='" . t($title) . "'></a>";
  }
  if($vars['element']['#field_name'] == 'field_tipo_de_producto') {
      $tipo = $vars['items']['0']['#markup'];
	  $vars['items']['0']['#markup'] = "<a class='tipo-" . $tipo . "' href='#' title='" . bastian_tipo_producto($tipo) . "'></a>";
  }
  if($vars['element']['#field_name'] == 'field_tipo_de_recurso') {
      $tipo = $vars['items']['0']['#markup'];
	  $vars['items']['0']['#markup'] = "<a class='tipo-" . $tipo . "' href='#' title='" . bastian_tipo_recurso($tipo) . "'></a>";
  }
}

function tb_wall_views_infinite_scroll_pager(&$variables) {
  $variables['img_path'] = base_path() . drupal_get_path('theme', 'tb_wall') . '/images/ajax-loader.gif' ;
  return theme_views_infinite_scroll_pager($variables);
}

function tb_wall_create_blank_image ($width, $height) {
  static $cache_data = false;
  static $cache_exists = false;
  if (!$cache_data) {
  	$cache = cache_get(__FUNCTION__ . ':tb_wall_lazyload_images');
  	if($cache && isset($cache->data)) {
  	  $cache_data = $cache->data;
  	}
  }
  if ($cache_data) {
  	if (isset($cache_data[$width][$height])) {
  	  if(!$cache_exists) {
  	  	if(file_exists($cache_data[$width][$height]['url'])) {
  	      $cache_exists = true;
          return $cache_data[$width][$height]['url'];
  	  	}
  	  }
  	  else {
        return $cache_data[$width][$height]['url'];
  	  }
    }
  }
  else {
  	$cache_data = array();
  }
  $folder_uri = "public://tb_wall_lazyload_images";
  if (!file_exists($folder_uri)) {
    mkdir($folder_uri, 0777);
  }

  $img = imagecreate($width, $height);
  $background = imagecolorallocate($img, 0, 0, 0);
  imagecolortransparent($img, $background);
  $file_uri = $folder_uri . "/" . $width . "x" . $height . ".png";
  $file_url = file_create_url($file_uri);
  imagepng($img, drupal_realpath($file_uri));
  $cache_data[$width][$height] = array('uri' => $file_uri, 'url' => $file_url);
  cache_set(__FUNCTION__ . ':tb_wall_lazyload_images', $cache_data);
  return $file_url;
}

function tb_wall_get_image_size($path) {
  static $cache_data = false;
  if (!$cache_data) {
  	$cache = cache_get(__FUNCTION__ . ':tb_wall_external_images');
  	if(isset($cache->data)) {
  	  $cache_data = $cache->data;
  	}
  }
  if ($cache_data && isset($cache_data[$path])) {
  	return $cache_data[$path];
  }
  $size = getimagesize($path);
  $cache_data[$path] = $size;
  cache_set(__FUNCTION__ . ':tb_wall_external_images', $cache_data);
  return $size;
	
}


function tb_wall_process_image(&$variables) {
  if (isset($variables['width']) && isset($variables['height'])) {
    $variables['attributes']['data-src'] = file_create_url($variables['path']);
    $file_url = tb_wall_create_blank_image($variables['width'], $variables['height']);
    $variables['path'] = $file_url;
  }
  else {
  	$size = tb_wall_get_image_size($variables['path']);
  	if(isset($size[0]) && isset($size[1])) {
  	  $variables['attributes']['data-src'] = file_create_url($variables['path']);
      $file_url = tb_wall_create_blank_image($size[0], $size[1]);
      $variables['path'] = $file_url;
  	}
  }
}

function tb_wall_image_formatter($variables) {
  $item = $variables['item'];
  $image = array(
    'path' => $item['uri'],
  );

  if (array_key_exists('alt', $item)) {
    $image['alt'] = $item['alt'];
  }

  if (isset($item['attributes'])) {
    $image['attributes'] = $item['attributes'];
  }

  if (isset($item['width']) && isset($item['height'])) {
    $image['width'] = $item['width'];
    $image['height'] = $item['height'];
  }

  // Do not output an empty 'title' attribute.
  if (isset($item['title']) && drupal_strlen($item['title']) > 0) {
    $image['title'] = $item['title'];
  }

  if ($variables['image_style']) {
    $image['style_name'] = $variables['image_style'];
    $output = theme('image_style', $image);
  }
  else {
    $output = theme('image', $image);
  }

  // The link path and link options are both optional, but for the options to be
  // processed, the link path must at least be an empty string.
  if (isset($variables['path']['path'])) {
    $path = $variables['path']['path'];
    $options = isset($variables['path']['options']) ? $variables['path']['options'] : array();
    $options += array('attributes' => array('class' => array()));
    $options['attributes']['class'][] = "";
    //$options['attributes']['class'][] = "colorbox-load";
    // When displaying an image inside a link, the html option must be TRUE.
    $options['html'] = TRUE;
    $output = l($output, $path, $options);
  }

  return $output;
}

/**
 * Theme function for thumbnails.
 */
function tb_wall_youtube_thumbnail($variables) {
  $id = $variables['video_id'];
  $style = $variables['image_style'];

  // Get YouTube settings - TODO is this needed?
  $size = variable_get('youtube_size', '420x315');
  $dimensions = youtube_get_dimensions($size);

  $files = variable_get('file_public_path', conf_path() . '/files');
  $youtube = variable_get('youtube_thumb_dir', 'youtube');
  $dest = $files . '/' . $youtube . '/' . $id . '.png';

  // Check to see if a thumbnail exists locally.
  if (!file_exists($dest)) {
    // Retrieve the image from YouTube.
    if (!youtube_get_remote_image($id)) {
      // Use the remote source if local copy fails.
      $src = youtube_build_remote_image_path($id);
      return theme('image', array('path' => $src));
    }
  }

  if ($style) {
    $uri = 'public://' . $youtube . '/' . $id . '.png';
    $image = theme('image_style', array('style_name' => $style, 'path' => $uri));
  }
  else {
    $path = $files . '/' . $youtube . '/' . $id . '.png';
    $image = theme('image', array('path' => $path));
  }

  // Check if an url path is provided
  if ($variables['image_link'] != NULL) {
    $url_path = $variables['image_link']['path'];
    $options = $variables['image_link']['options'];
    $options += array('attributes' => array('class' => array()));
    $options['attributes']['class'][] = "colorbox-load";
    $image = l($image, $url_path, $options);
  }

  return $image;
}


/* FLAG IMAGE */
function tb_wall_preprocess_flag(&$vars) {
    $state = ($vars['action'] == 'flag' ? 'off' : 'on');
    $image_file = $vars['directory'] . '/images/flag-' . $vars['flag']->name . '-' . $state . '.png';
    // Uncomment the following line when debugging.
//    drupal_set_message("Flag is looking for '$image_file'...");
    if (file_exists($image_file)) {
      $vars['link_text'] = theme_image(array('path' => $image_file, 'attributes' =>
          array('class' => array('flag-' . $vars['flag']->name . '-' . $state))));
    }
}



/* USER MENU LINK CHANGE */
function tb_wall_superfish_build($variables) {
  $output = array('content' => '');
  $id = $variables['id'];
  $menu = $variables['menu'];
  $depth = $variables['depth'];
  $trail = $variables['trail'];
  // Keep $sfsettings untouched as we need to pass it to the child menus.
  $settings = $sfsettings = $variables['sfsettings'];
  $megamenu = $megamenu_below = $settings['megamenu'];
  $total_children = $parent_children = $single_children = 0;
  $i = 1;

  // Reckon the total number of available menu items.
  foreach ($menu as $menu_item) {
    if (!isset($menu_item['link']['hidden']) || $menu_item['link']['hidden'] == 0) {
      $total_children++;
    }
  }

  foreach ($menu as $menu_item) {

	/*  BASTIAN */
	if ($menu_item['link']['menu_name'] == 'user-menu') {
		if ($menu_item['link']['title'] == '[current-user:field-nombre-y-apellidos]') {
			$menu_item['link']['title'] = t('Login / Registro');
			$menu_item['link']['link_title'] = t('Login / Registro');
		}
	}
	/*  BASTIAN */
	
	
    $show_children = $megamenu_wrapper = $megamenu_column = $megamenu_content = FALSE;
    $item_class = $link_options = $link_class = array();
    $mlid = $menu_item['link']['mlid'];

    if (!isset($menu_item['link']['hidden']) || $menu_item['link']['hidden'] == 0) {
      $item_class[] = ($trail && in_array($mlid, $trail)) ? 'active-trail' : '';

      // Add helper classes to the menu items and hyperlinks.
      $settings['firstlast'] = ($settings['dfirstlast'] == 1 && $total_children == 1) ? 0 : $settings['firstlast'];
      $item_class[] = ($settings['firstlast'] == 1) ? (($i == 1 && $i == $total_children) ? 'firstandlast' : (($i == 1) ? 'first' : (($i == $total_children) ? 'last' : 'middle'))) : '';
      $settings['zebra'] = ($settings['dzebra'] == 1 && $total_children == 1) ? 0 : $settings['zebra'];
      $item_class[] = ($settings['zebra'] == 1) ? (($i % 2) ? 'odd' : 'even') : '';
      $item_class[] = ($settings['itemcount'] == 1) ? 'sf-item-' . $i : '';
      $item_class[] = ($settings['itemdepth'] == 1) ? 'sf-depth-' . $menu_item['link']['depth'] : '';
      $link_class[] = ($settings['itemdepth'] == 1) ? 'sf-depth-' . $menu_item['link']['depth'] : '';
      $item_class[] = ($settings['liclass']) ? $settings['liclass'] : '';
      if (strpos($settings['hlclass'], ' ')) {
        $l = explode(' ', $settings['hlclass']);
        foreach ($l as $c) {
          $link_class[] = $c;
        }
      }
      else {
        $link_class[] = $settings['hlclass'];
      }
      $i++;

      // Hide hyperlink descriptions ("title" attribute).
      if ($settings['hidelinkdescription'] == 1) {
        unset($menu_item['link']['localized_options']['attributes']['title']);
      }

      // Insert hyperlink description ("title" attribute) into the text.
      $show_linkdescription = ($settings['linkdescription'] == 1 && !empty($menu_item['link']['localized_options']['attributes']['title'])) ? TRUE : FALSE;
      if ($show_linkdescription) {
        if (!empty($settings['hldmenus'])) {
          $show_linkdescription = (is_array($settings['hldmenus'])) ? ((in_array($mlid, $settings['hldmenus'])) ? TRUE : FALSE) : (($mlid == $settings['hldmenus']) ? TRUE : FALSE);
        }
        if (!empty($settings['hldexclude'])) {
          $show_linkdescription = (is_array($settings['hldexclude'])) ? ((in_array($mlid, $settings['hldexclude'])) ? FALSE : $show_linkdescription) : (($settings['hldexclude'] == $mlid) ? FALSE : $show_linkdescription);
        }
        if ($show_linkdescription) {
          $menu_item['link']['title'] .= ' <span class="sf-description">';
          $menu_item['link']['title'] .= (!empty($menu_item['link']['localized_options']['attributes']['title'])) ? $menu_item['link']['localized_options']['attributes']['title'] : array();
          $menu_item['link']['title'] .= '</span>';
          $link_options['html'] = TRUE;
        }
      }

      // Add custom HTML codes around the menu items.
      if ($sfsettings['wrapul'] && strpos($sfsettings['wrapul'], ',') !== FALSE) {
        $wul = explode(',', $sfsettings['wrapul']);
        // In case you just wanted to add something after the element.
        if (drupal_substr($sfsettings['wrapul'], 0) == ',') {
          array_unshift($wul, '');
        }
      }
      else {
        $wul = array();
      }

      // Add custom HTML codes around the hyperlinks.
      if ($settings['wraphl'] && strpos($settings['wraphl'], ',') !== FALSE) {
        $whl = explode(',', $settings['wraphl']);
        // The same as above
        if (drupal_substr($settings['wraphl'], 0) == ',') {
          array_unshift($whl, '');
        }
      }
      else {
        $whl = array();
      }

      // Add custom HTML codes around the hyperlinks text.
      if ($settings['wraphlt'] && strpos($settings['wraphlt'], ',') !== FALSE) {
        $whlt = explode(',', $settings['wraphlt']);
        // The same as above
        if (drupal_substr($settings['wraphlt'], 0) == ',') {
          array_unshift($whlt, '');
        }
        $menu_item['link']['title'] = $whlt[0] . check_plain($menu_item['link']['title']) . $whlt[1];
        $link_options['html'] = TRUE;
      }

      $expanded = ($sfsettings['expanded'] == 1) ? (($menu_item['link']['expanded'] == 1) ? TRUE : FALSE) : TRUE;

      if (!empty($menu_item['link']['has_children']) && !empty($menu_item['below']) && $depth != 0 && $expanded === TRUE) {

        // Megamenu is still beta, there is a good chance much of this will be changed.
        if (!empty($settings['megamenu_exclude'])) {
          if (is_array($settings['megamenu_exclude'])) {
            $megamenu_below = (in_array($mlid, $settings['megamenu_exclude'])) ? 0 : $megamenu;
          }
          else {
            $megamenu_below = ($settings['megamenu_exclude'] == $mlid) ? 0 : $megamenu;
          }
          // Send the result to the sub-menu.
          $sfsettings['megamenu'] = $megamenu_below;
        }
        if ($megamenu_below == 1) {
          $megamenu_wrapper = ($menu_item['link']['depth'] == $settings['megamenu_depth']) ? TRUE : FALSE;
          $megamenu_column = ($menu_item['link']['depth'] == $settings['megamenu_depth'] + 1) ? TRUE : FALSE;
          $megamenu_content = ($menu_item['link']['depth'] >= $settings['megamenu_depth'] && $menu_item['link']['depth'] <= $settings['megamenu_levels']) ? TRUE : FALSE;
        }
        // Render the sub-menu.
        $var = array(
          'id' => $id,
          'menu' => $menu_item['below'],
          'depth' => $depth,
          'trail' => $trail,
          'sfsettings' => $sfsettings
        );
        $children = theme('superfish_build', $var);
        // Check to see whether it should be displayed.
        $show_children = (($menu_item['link']['depth'] <= $depth || $depth == -1) && $children['content']) ? TRUE : FALSE;
        if ($show_children) {
          // Add item counter classes.
          if ($settings['itemcounter']) {
            $item_class[] = 'sf-total-children-' . $children['total_children'];
            $item_class[] = 'sf-parent-children-' . $children['parent_children'];
            $item_class[] = 'sf-single-children-' . $children['single_children'];
          }
          // More helper classes.
          $item_class[] = ($megamenu_column) ? 'sf-megamenu-column' : '';
          $item_class[] = $link_class[] = 'menuparent';
        }
        $parent_children++;
      }
      else {
        $item_class[] = 'sf-no-children';
        $single_children++;
      }

      $item_class = implode(' ', array_remove_empty($item_class));

      if (isset($menu_item['link']['localized_options']['attributes']['class'])) {
        $link_class_current = $menu_item['link']['localized_options']['attributes']['class'];
        $link_class = array_merge($link_class_current, array_remove_empty($link_class));
      }
      $menu_item['link']['localized_options']['attributes']['class'] = array_remove_empty($link_class);

      // The Context module uses theme_menu_link, Superfish does not, this is why we have to do this.
      if (function_exists('context_active_contexts')) {
        if ($contexts = context_active_contexts()) {
          foreach ($contexts as $context) {
            if ((isset($context->reactions['menu']))) {
              if ($menu_item['link']['href'] == $context->reactions['menu']) {
                $menu_item['link']['localized_options']['attributes']['class'][] = 'active';
              }
            }
          }
        }
      }

      $link_options += $menu_item['link']['localized_options'];

      // Render the menu item.
      // Should a theme be used for menu items?
      if ($settings['use_item_theme']) {
        $item_variables = array(
          'element' => array(
            'attributes' => array(
              'id' => 'menu-' . $mlid . '-' . $id,
              'class' => trim($item_class),
            ),
            'below' => ($show_children) ? $children['content'] : NULL,
            'item' => $menu_item,
            'localized_options' => $link_options,
          ),
          'properties' => array(
            'megamenu' => array(
              'megamenu_column' => $megamenu_column,
              'megamenu_wrapper' => $megamenu_wrapper,
              'megamenu_content' => $megamenu_content,
            ),
            'use_link_theme' => $settings['use_link_theme'],
            'wrapper' => $whl,
          ),
        );
        $output['content'] .= theme('superfish_menu_item', $item_variables);
      }
      else {
        $output['content'] .= '<li id="menu-' . $mlid . '-' . $id . '"';
        $output['content'] .= ($item_class) ? ' class="' . trim($item_class) . '">' : '>';
        $output['content'] .= ($megamenu_column) ? '<div class="sf-megamenu-column">' : '';
        $output['content'] .= isset($whl[0]) ? $whl[0] : '';
        if ($settings['use_link_theme']) {
          $link_variables = array(
            'menu_item' => $menu_item,
            'link_options' => $link_options,
          );
          $output['content'] .= theme('superfish_menu_item_link', $link_variables);
        }
        else {
          $output['content'] .= l($menu_item['link']['title'], $menu_item['link']['href'], $link_options);
        }
        $output['content'] .= isset($whl[1]) ? $whl[1] : '';
        $output['content'] .= ($megamenu_wrapper) ? '<ul class="sf-megamenu"><li class="sf-megamenu-wrapper ' . $item_class . '">' : '';
        $output['content'] .= ($show_children) ? (isset($wul[0]) ? $wul[0] : '') : '';
        $output['content'] .= ($show_children) ? (($megamenu_content) ? '<ol>' : '<ul>') : '';
        $output['content'] .= ($show_children) ? $children['content'] : '';
        $output['content'] .= ($show_children) ? (($megamenu_content) ? '</ol>' : '</ul>') : '';
        $output['content'] .= ($show_children) ? (isset($wul[1]) ? $wul[1] : '') : '';
        $output['content'] .= ($megamenu_wrapper) ? '</li></ul>' : '';
        $output['content'] .= ($megamenu_column) ? '</div>' : '';
        $output['content'] .= '</li>';
      }
    }
  }
  $output['total_children'] = $total_children;
  $output['parent_children'] = $parent_children;
  $output['single_children'] = $single_children;
  return $output;
}


function tb_wall_term_tree_list($variables) {
  $element = &$variables['element'];
  $data = &$element['#data'];

  $tree = array();

  // For each selected term:
  foreach ($data as $item) {
    // Loop if the term ID is not zero:
    $values = array();
    $tid = $item['tid'];
    $original_tid = $tid;
    while ($tid != 0) {
      // Unshift the term onto an array
      array_unshift($values, $tid);

      // Repeat with parent term
      $tid = _term_reference_tree_get_parent($tid);
    }

    $current = &$tree;
    // For each term in the above array:
    foreach ($values as $tid) {
      // current[children][term_id] = new array
      if (!isset($current['children'][$tid])) {
        $current['children'][$tid] = array('selected' => FALSE);
      }

      // If this is the last value in the array, tree[children][term_id][selected] = true
      if ($tid == $original_tid) {
        $current['children'][$tid]['selected'] = TRUE;
      }

      $current['children'][$tid]['tid'] = $tid;
      $current = &$current['children'][$tid];
    }
  }

  $output = "<div class='term-tree-list'>";
  $output .= tb_wall_term_reference_tree_output_list_level($element, $tree);
  $output .= "</div>";
  return $output;
}


function tb_wall_term_reference_tree_output_list_level(&$element, &$tree) {
//  dpm($element);
  if (isset($tree['children']) && is_array($tree['children']) && count($tree['children'] > 0)) {
    $output = '<ul class="term">';
    $settings = $element['#display']['settings'];
    $tokens_selected = $settings['token_display_selected'];
    $tokens_unselected = ($settings['token_display_unselected'] != '') ? $settings['token_display_unselected'] : $tokens_selected;

    $taxonomy_term_info = entity_get_info('taxonomy_term');
    foreach ($tree['children'] as &$item) {
      $term = $taxonomy_term_info['load hook']($item['tid']);
      $uri = $taxonomy_term_info['uri callback']($term);
      $uri['options']['html'] = TRUE;
      $class = $item['selected'] ? 'selected' : 'unselected';
      $output .= "<li class='$class'>";

      if ($tokens_selected != '' && module_exists('token')) {
        $replace = $item['selected'] ? $tokens_selected : $tokens_unselected;
        $output .= token_replace($replace, array('term' => $term), array('clear' => TRUE));
      }
	  // SI LA TAXONOM�A TIENE IMAGEN, LA MOSTRAMOS
      else {
	    if ($term->field_image) {
//		   $output .= theme('image', array('path' =>$term->field_image['und'][0]['uri']));
		   $imagen = theme('image', array('path' =>$term->field_image['und'][0]['uri'], 'title' => $term->name));

		   if (bastian_is_premium()) {
//		   if (in_array("Premium", $user->roles)) {
			   	$link = l($imagen, "taxonomy/term/".$term->tid, array('html' => true));
		   }
		   else {
			   	$link = $imagen;
		   }

		   $output .= "<div class='taxonomy-tree-image'>" . $link . "</div>";
//		   $output .= "<div class='taxonomy-tree-image'>" . l($imagen, "taxonomy/term/".$term->tid, array('html' => true)) . "</div>";
		}
//		else
        $output .= "<div class='taxonomy-tree-text'>" . l(entity_label('taxonomy_term', $term), $uri['path'], $uri['options']) . "</div>";
      }
      if (isset($item['children'])) {
        $output .= _term_reference_tree_output_list_level($element, $item);
      }
      $output .= "</li>";
    }

    $output .= '</ul>';
    return $output;
  }
}


function tb_wall_form_comment_form_alter(&$form, &$form_state) {
  $form['actions']['submit']['#value'] = t('Send');
}



