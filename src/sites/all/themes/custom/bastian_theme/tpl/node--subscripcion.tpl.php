<div id="article-<?php print $node->nid; ?>" class="article node-subs <?php print $classes; ?> clearfix"<?php print $attributes; ?>>
        <div class="subscription">
            <div class="subs-info">
            <h2 class="title"><?php print t("¿Porqué suscribirse al Los Cuentos de Bastian?"); ?></h2>
            <ul class="subs-settings">
                <li>
                    <span class="icon glasses"></span>
                    <p><?php print t("Todos los recursos 100% especializados"); ?></p>
                </li>
                <li>
                    <span class="icon book"></span>
                    <p><?php print t("Material exclusivo para los tuyos"); ?></p>
                </li>
                <li>
                    <span class="icon face"></span>
                    <p><?php print t("Disfrutar tranquilo con tu familia"); ?></p>
                </li>
            </ul>
            
            <h2 class="title"><?php print t("¿Qué ventajas tiene?"); ?></h2>
            <table>
                <thead>
                    <tr>
                        <th></th>
                        <th><?php print t("Usuario solo registrado"); ?></th>
                        <th><?php print t("Usuario con suscripción"); ?></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="index"><?php print t("Contenido"); ?></td>
                        <td><?php print t("Básico"); ?></td>
                        <td><?php print t("Experto"); ?></td>
                    </tr>
                    <tr>
                        <td class="index"><?php print t("Descuentos en libros y box"); ?></td>
                        <td></td>
                        <td>5%</td>
                    </tr>
                    <tr>
                        <td class="index"><?php print t("Descuentos en otros productos"); ?></td>
                        <td></td>
                        <td>10%</td>
                    </tr>
                    <tr>
                        <td class="index"><?php print t("Juegos Descargables"); ?></td>
                        <td></td>
                        <td><span class="tick"></span></td>
                    </tr>
                    <tr>
                        <td class="index"><?php print t("Lecturas Gratuitas"); ?></td>
                        <td></td>
                        <td><span class="tick"></span></td>
                    </tr>
                    <tr>
                        <td class="index"><?php print t("Búsqueda especializada"); ?></td>
                        <td></td>
                        <td><span class="tick"></span></td>
                    </tr>
                </tbody>
            </table>
                    
            
            
            <h2 class="title"><?php print t("¡Escoge tu plan de suscripción!"); ?></h2>
            <?php $block = block_load('bastian', 'subscription');
            print drupal_render(_block_get_renderable_array(_block_render_blocks(array($block)))); ?>
            
        </div>
    </div>


  <?php
   if ($links = render($content['links'])): ?>
    <div class="menu node-links clearfix"><?php //print $links; ?></div>
  <?php endif; ?>

  <?php print render($content['comments']); ?>
</div>
