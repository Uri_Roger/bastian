<?php /* <a href="<?php print url('<front>'); ?>"><img style="padding: 2em 2em 1em;" src="/<?php print path_to_theme().'/images/header-logo.png'; ?>"></a>*/ ?>
<?php $nexts = bastian_prev_next_node_based_on_current_filter($node->nid);

if ($nexts['prev'])
    print l('Prev', 'node/'.$nexts['prev'], array('attributes' => array('class' => 'prev-link')));
if ($nexts['next']) {
    print l('Next', 'node/'.$nexts['next'], array('attributes' => array('class' => 'next-link')));
}
print l('Home', '<front>', array('attributes' => array('class' => 'home-link')));
bastian_prev_next_node_based_on_current_filter();
 ?>
<div id="article-<?php print $node->nid; ?>" class="article recurso <?php print $classes; ?> clearfix"<?php print $attributes; ?>>

    <?php if ($bastian_theme_first_field): ?>
        <div class="first-field">
            <?php print($bastian_theme_first_field); ?>
            <?php print render($content['group_contenido']['group_franja_4']); ?>
        </div>
    <?php endif; ?>

    <div<?php print $content_attributes; ?>>
        <?php
        // We hide the comments and links now so that we can render them later.
        hide($content['comments']);
        hide($content['links']);
        global $user;
        global $base_url;
        //      dpm($content);

        //print render($content['field_id_card_type']);
        //echo "<pre>";
       //var_dump(($content['group_inferior']['group_add_to_cart']));
       // die();
        ?>
        <div id="node_product_full_group_superior" class="group-superior field-group-div">


            <?php print render($content['group_superior']['group_product_left']); ?>

            <div id="node_product_full_group_product_right" class="group-product-right field-group-div">
                <div class="field field-name-field-titulo field-type-computed field-label-hidden">
                    <?php
                        print render($content['group_inferior']['group_texto']['field_tipo_de_contenido']);
                        print render($content['group_inferior']['group_texto']['field_tipo_de_recurso']);
                        print render($content['group_inferior']['group_texto']['field_tipo_de_producto']);
                        print render($content['group_inferior']['group_texto']['field_titulo']);
                        print render($content['group_inferior']['group_texto']['field_subtitulo']);						
                    ?>
                    <div class="subtitulo">
                        <?php global $language; ?>
                        <p><?php print $content['field_subtitulo']["#object"]->field_subtitulo[$language->language][0]['value']; ?></p>
                    </div>
                </div>
                <div class="stadistics">
                    <?php  print render($content['group_inferior']['group_texto']['field_rating']); ?>
                </div>
                <div class="age-product">
                    <?php print render($content['group_inferior']['group_texto']['field_edad']); ?>
                </div>

                <div class="required-fields group-caracteristicas">
                    <?php
                        print render($content['group_superior']['group_product_right']['group_detalles']['group_caracteristicas']['field_idioma']);
                        print render($content['group_superior']['group_product_right']['group_detalles']['group_caracteristicas']['field_autor']);
                        print render($content['group_superior']['group_product_right']['group_detalles']['group_caracteristicas']['field_colaborador']);
                        print render($content['group_superior']['group_product_right']['group_detalles']['group_caracteristicas']['field_ilustrador']);
                        print render($content['group_superior']['group_product_right']['group_detalles']['group_caracteristicas']['field_del_libro']);
                        print render($content['group_superior']['group_product_right']['group_detalles']['group_caracteristicas']['field_objetivos']);
                        print render($content['group_superior']['group_product_right']['group_detalles']['group_caracteristicas']['field_medidas']);
                        print render($content['group_superior']['group_product_right']['group_detalles']['group_caracteristicas']['field_mides']);
                        print render($content['group_superior']['group_product_right']['group_detalles']['group_caracteristicas']['field_material_producto']);
                        print render($content['group_superior']['group_product_right']['group_detalles']['group_caracteristicas']['field_numero_jugadores']);
                        //print render($content['group_superior']['group_product_right']['group_detalles']['group_caracteristicas']['field_contenido']);
                        print render($content['group_superior']['group_product_right']['group_detalles']['group_caracteristicas']['field_material']);
                        if (isset($content['group_superior']['group_product_right']['group_detalles']['group_caracteristicas']['field_duracion'])) {
                            print render($content['group_superior']['group_product_right']['group_detalles']['group_caracteristicas']['field_duracion']);
                        }
                        print render($content['group_superior']['group_product_right']['group_detalles']['group_caracteristicas']['field_tipo_de_juego']);
                        print render($content['group_superior']['group_product_right']['group_detalles']['group_caracteristicas']['field_premio']);
                        print render($content['group_superior']['group_product_right']['group_detalles']['group_caracteristicas']['field_isbn']);
                        print render($content['group_superior']['group_product_right']['group_detalles']['group_caracteristicas']['field_contenido']);
                        print render($content['group_superior']['group_product_right']['group_detalles']['group_caracteristicas']['field_editorial']);
                        print render($content['group_superior']['group_product_right']['group_detalles']['group_caracteristicas']['field_inspirado_en_el_libro']);
                        print render($content['group_superior']['group_product_right']['group_detalles']['group_caracteristicas']['field_elaborado']);
                        print render($content['group_superior']['group_product_right']['group_detalles']['group_caracteristicas']['field_formato']);
                        print render($content['group_superior']['group_product_right']['group_detalles']['group_caracteristicas']['field_ultima_edicion']);
                        print render($content['group_superior']['group_product_right']['group_detalles']['group_caracteristicas']['field_paginas']);
                        print render($content['group_superior']['group_product_right']['group_detalles']['group_caracteristicas']['field_desenvolupador']);

                        print render($content['affiliate']);



						// LINK AFILIACIÓN
						$tipos = array('product','product_kit','otros_productos');
						if (in_array('Afiliado',$user->roles) && in_array($node->type,$tipos)) {
							if(isset($node->field_imagen_pequena['und'][0]['uri'])){ 
								$imagen_pequena = image_style_url('thumbnail', $node->field_imagen_pequena['und'][0]['uri']);
							} 
							$afiliacion  = '<div id="afiliacion">';
							$afiliacion .= '<a id="afiliacion-enlace" href="#">Ver enlace de afiliación</a>';

							$afiliacion .= '<textarea id="afiliacion-textarea">';
							$afiliacion .= '<div id="prodcontain"><a href="https://loscuentosdebastian.com/affiliate/' . $user->uid . '/node/' . $node->nid . '" target="_blank">';
							$afiliacion .= '<img src="' . $imagen_pequena . '">';
							$afiliacion .= '<br><div id="title">' . $node->title . '</div></a></div>';
							$afiliacion .= '</textarea>';
							$afiliacion .= '</div>';
							print $afiliacion;
						}




                        $node_obj = node_load(arg(1));
                        $imatge1 = (field_get_items('node', $node_obj, 'field_imatge_disponible_en'));
                        $imatge1 = $imatge1[0]['uri'];
                        $imatge2 = (field_get_items('node', $node_obj, 'field_imagen_disponible_en_2'));
                        $imatge2 = $imatge2[0]['uri'];
                        $imatge3 = (field_get_items('node', $node_obj, 'field_imagen_disponible_en_3'));
                        $imatge3 = $imatge3[0]['uri'];
                        $link1 = (field_get_items('node', $node_obj, 'field_link_disponible_en'));
                        $link1 = $link1[0]['url'];
                        $link2 = (field_get_items('node', $node_obj, 'field_link_disponible_en_2'));
                        $link2 = $link2[0]['url'];
                        $link3 = (field_get_items('node', $node_obj, 'field_link_disponible_en_3'));
                        $link3 = $link3[0]['url'];
                        
                        

                        if (isset($imatge1) && isset($link1)) : ?>
                            <div class="field field-name-field-disponible field-type-text field-label-inlinec clearfix field-label-inline">
                                <div class="field-label"><?php print t('Disponible en'); ?></div>
                                <div class="field-items">
                                    <div class="field-item even"><a href="<?php print $link1; ?>"><img src="<?php print image_style_url('desarrollador', $imatge1); ?>"></a></div>
                                    <?php if (isset($imatge2) && isset($link2)) : ?>
                                        <div class="field-item even"><a href="<?php print $link2; ?>"><img src="<?php print image_style_url('desarrollador', $imatge2); ?>"></a></div>
                                    <?php endif; ?>
                                    <?php if (isset($imatge3) && isset($link3)) : ?>
                                        <div class="field-item even"><a href="<?php print $link3; ?>"><img src="<?php print image_style_url('desarrollador', $imatge3); ?>"></a></div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        <?php endif; ?>
                        <?php print render($content['group_superior']['group_product_right']['group_detalles']['group_caracteristicas']['field_pru_valo_en']); ?>

                    	<?php print render($content['group_superior']['group_product_left']['group_compartir']['flag_wish']);	?>

                    	<?php print render($content['group_superior']['group_product_left']['group_compartir']['sharethis']);	?>


                </div>
                <div class="price-item">
                    <?php print render($content['group_inferior']['group_add_to_cart']['display_price']); ?>
                    <?php print render($content['group_inferior']['group_add_to_cart']['field_precio_descuento']); ?>


                    <?php // MOSTRAMOS EL NUEVO PRECIO CON EL 5% DE DESCUENTO, Y EL ANTIGUO EN ROJO TACHADO
						drupal_add_js("
							jQuery(document).ready(function () {
								mostrarPrecioDescuento(false);
/*								var priceIni = jQuery('.group-product-right .display-price .uc-price').text().slice(0,-1);
								var descuento = jQuery('.group-product-right .field-name-field-precio-descuento .field-item').text();
								if (descuento != 'no') {
									var price = priceIni.replace(',','.');
									var price_desc = parseFloat(price) - (parseFloat(price) * 0.05);
									console.log(price,parseFloat(price),price_desc);
									price_desc = price_desc.toFixed(2).replace('.', ',');
									console.log('price_desc:'+price_desc);
									jQuery('.group-product-right .field-name-field-precio-descuento .field-item').prepend(price_desc);
									jQuery('.group-product-right .field-name-field-precio-descuento .field-item').wrapInner('<span class=\'uc-price precio-descuento\'></span>');
									jQuery('.group-product-right .field-name-field-precio-descuento .precio-descuento').insertAfter(jQuery('.group-product-right .display-price .uc-price'));
								}
								else {
									console.log(descuento);
									jQuery('.group-product-right .field-name-field-precio-descuento').hide();
									jQuery('.group-product-right .display-price .uc-price').addClass('precio-descuento');
								}		*/
							}
						);", "inline");
					?>
                </div>

                <div class="select-language">
                    <?php //if($content['group_inferior']['group_add_to_cart']['add_to_cart']['#form']['attributes']['#theme']) { ?>
<!--                        <label class="select-language-label"> Selecciona tu idioma: </label>	-->
                    <?php  //} ?>
                    <?php print render($content['group_inferior']['group_add_to_cart']); ?>
                </div>





            <?php print render($content['group_superior']['group_product_right']['group_tags']); ?>
            <?php print render($content['group_superior']['group_product_right']['field_primer_capitulo']); ?>
            </div>
            <div id="node_product_full_group_inferior" class="group-inferior field-group-div">
            <?php //if (in_array('Premium',$user->roles)): ?>
                <?php print render($content['group_superior']['group_product_right']['group_detalles']['group_caracteristicas']['group_ambito']); ?>
            <?php //endif; ?>
            <div class="description-recurso">
<!--           <div class="field-label"><?php //print t('Así te lo contamos'); ?> </div>	-->
                <?php print render($content['group_inferior']['group_texto']['body']); ?>
                <?php print render($content['group_inferior']['group_texto']['field_opinion_bastian']); ?>
                <div class="video-content">
                    <?php print render($content['group_inferior']['group_texto']['field_video']); ?>
                </div>
                <?php print render($content['group_inferior']['group_texto']['field_pedagogia']); ?>
                <?php if (in_array('Premium',$user->roles)): ?>
                    <?php print render($content['group_inferior']['group_texto']['field_archivo']); ?>
                <?php endif; ?>
            </div>
        </div>
        </div>


	    <?php 	// PARA LECTURAS Y PASATIEMPOS, AÑADIMOS UNA CLASE CON INTERLINEADO MÁS AMPLIO
				if ($content['group_inferior']['group_texto']['field_tipo_de_recurso']['#items'][0]['value'] == "L" ||
					$content['group_inferior']['group_texto']['field_tipo_de_recurso']['#items'][0]['value'] == "H") {
					drupal_add_js("
						jQuery(document).ready(function () {
							jQuery('.field-name-field-opinion-bastian .field-item').wrap('<div class=\"texto-ancho\"></div>');
						}
					);", "inline");

					// PARA LECTURAS, AÑADIMOS LA LETRA INICIAL EN TAMAÑO GRANDE
					if ($content['group_inferior']['group_texto']['field_tipo_de_recurso']['#items'][0]['value'] == "L") {
						drupal_add_js("
							jQuery(document).ready(function () {
								jQuery('.texto-ancho .field-item').wrap('<div class=\"drop-caps\"></div>');
							}
						);", "inline");
					}


	    		}				?>
		



    </div>

    <?php
    if ($links = render($content['links'])): ?>
        <div class="menu node-links clearfix"><?php //print $links; ?></div>
    <?php endif; ?>

    <?php print render($content['comments']); ?>
</div>


<?php
function pn($var = "no var selected", $die = true) {
    if ($_SERVER['REMOTE_ADDR'] == '10.0.0.237' || $_SERVER['REMOTE_ADDR'] == '127.0.0.1') {
        // Do what you wanna do!
        echo '<pre>';
        var_dump($var);
        echo '</pre>';
        if ($die) die();
    }
}

?>