var locale = {
  SUBSCRITO: {
    ca: "Ja ets premium",
    es: "Ya eres premium",
    en: "Already premium"
  },
  CANTIDAD: {
    ca: "Cantitat",
    es: "Cantidad",
    en: "Quantity"
  },
  SUBSCRIBE: {
      ca: "Vols saber més? Fes-te de Bastian i gaudeix de tots els continguts.",
      es: "Accede a todos los contenidos por sólo 2,99€ al mes. ¡Suscríbete!",
      en: "Accede a todos los contenidos por sólo 2,99€ al mes. ¡Suscríbete!"
  },
  TRAMITAR: {
      ca: "Mètode de pagament",
      es: "Forma de pago",
      en: "Forma de pago"
  },
  TARGETA: {
      ca: "Dades de la targeta",
      es: "Datos de la targeta",
      en: "Datos de la targeta"
  },
  OPINION: {
      ca: "opinions",
      es: "opiniones",
      en: "opinions"
  },
  SELECIDIOMA: {
      ca: "Selecciona el teu idioma:",
      es: "Selecciona tu idioma:",
      en: "Select your language:"
  },
  ETIQUETES: {
      ca: "Etiquetes",
      es: "Etiquetas",
      en: "Tags"
  },
  FB_PRE: {
      ca: "O",
      es: "O bien",
      en: "Or"
  },
  BASTIAN_LOGIN: {
      ca: 'Ets de Los Cuentos de Bastian?',
      es: '¿Eres de Los Cuentos de Bastian?',
      en: '¿Eres de Los Cuentos de Bastian?'
  },
  DESELECCIONAR: {
      ca: 'Deseleccionar tot el filtre',
      es: 'Deseleccionar todo el filtro',
      en: 'Unselect all filters'
  },
  PERSONALIZAR: {
      ca: "Vols personalitzar aquest llibre?",
      es: "¿Quieres personalizar este libro?",
      en: "Do you want to custom this book?"
  },
  DESEO: {
	  ca: 'Afegir a llista de desitjos',
      es: 'Añadir a lista de deseos',
      en: 'Add to wish list'
  },
  UNDESEO: {
	  ca: 'Treure de la llista de desitjos',
      es: 'Quitar de la lista de deseos',
      en: 'Remove from wish list'
  }
}
var config = {
  LANGUAGE: 'es',
  THEME_URL: '/sites/all/themes/bastian_theme/',
  WINDOW_MEASURES: [],
  RESIZE_THRESHOLD: 20 //miliseconds
};


(function ($) {
  Drupal.TBWall = Drupal.TBWall || {};

// BASTIAN - ANCHO DE BLOQUE EN LA HOME
  Drupal.TBWall.columnWidth = 250;
  Drupal.TBWall.sidebarColumnWidth = 250;
//  Drupal.TBWall.columnWidth = 200;
//  Drupal.TBWall.sidebarColumnWidth = 200;
// BASTIAN - ANCHO DE BLOQUE EN LA HOME

  Drupal.TBWall.toolbarHeight = -1;
  Drupal.TBWall.headerHeight = -1;
  Drupal.TBWall.sidebarIScroll = false;
  Drupal.TBWall.popupIScroll = false;
  Drupal.TBWall.supportedScreens = [0.5, 479.5, 719.5, 959.5, 1049.5, 1235.5, 1585.5, 1890.5];
  Drupal.TBWall.screens = ['empty', 'mobile-vertical', 'mobile', 'tablet-vertical', 'tablet', 'normal-screen', 'wide', 'wide-extra', 'hd'];
  Drupal.TBWall.currentScreen = "";
  Drupal.TBWall.currentWidth = -1;
  Drupal.TBWall.IE8 = navigator.userAgent.search(/MSIE 8.0/) != -1;
  Drupal.TBWall.toolbar = false;
  Drupal.TBWall.masonry_container = false;
  Drupal.TBWall.masonry_sidebar_container = false;

  Drupal.behaviors.actionTBWall = {
    attach: function (context) {
      $(window).scroll(function() {
        Drupal.TBWall.btnToTop();
        Drupal.TBWall.loadActualImages();
      });

      $(window).resize(function(){
        $('body').css({'padding-top': Drupal.TBWall.toolbar ? (Drupal.TBWall.toolbar.height() - (Drupal.TBWall.IE8 ? 10 : 0)) : 0});
        Drupal.TBWall.loadActualImages();
        Drupal.TBWall.updateResponsiveMenu();
        Drupal.TBWall.updateScrollSize();
        Drupal.TBWall.mobilePopup();
        Drupal.TBWall.currentWidth = window.innerWidth ? window.innerWidth : $(window).width();
        Drupal.TBWall.updateMasonryMainWidth(true);
        Drupal.TBWall.updateMasonrySidebarWidth(true);
        Drupal.TBWall.ie8Resize();
        window.setTimeout(function() {
          $('#modalBackdrop').css('width', '100%');
        }, 500);
        var windowWidth = window.innerWidth ? window.innerWidth : $(window).width();
        if(windowWidth > Drupal.TBWall.supportedScreens[4]){
          $('#main-wrapper .page-main-inner, #sidebar-first-wrapper .grid-inner').matchHeights();
        }
        if (jQuery('body').width() > 550) {
          $('.region.region-header .block-views').css('left', (jQuery('body').width()/2)-(545/2));
        }
      });
    }
  };

  Drupal.TBWall.ie8Resize = function() {
    if(Drupal.TBWall.IE8) {
      current_screen = "";
      for (i = 0; i < Drupal.TBWall.supportedScreens.length; i ++) {
        if (Drupal.TBWall.currentWidth < Drupal.TBWall.supportedScreens[i]) {
          current_screen = Drupal.TBWall.screens[i];
          break;
        }
      }
      if (current_screen != Drupal.TBWall.currentScreen) {
        if (current_screen == 'normal-screen') {
          $style = '<style type="text/css" class="normal-screen-ie8">.sharethis-buttons { position: inherit; bottom: 0px; right: auto; left: 0px; margin-bottom: 10px;}'
          $style += ".gallery-slides, .gallery-thumbs { width: auto !important; }</style>";
          $("#page").append($($style));
        }
        else {
          $("#page style.normal-screen-ie8").remove();
        }
        Drupal.TBWall.currentScreen = current_screen;
      }
    }
  }

  Drupal.TBWall.updateMainColumnWidth = function() {
    Drupal.TBWall.masonry_container.addClass('masonry-reload');
    var container_width = Drupal.TBWall.masonry_container.width();
    var number_column = Math.round(container_width / Drupal.TBWall.columnWidth);
    var column_width = Math.floor(container_width / number_column);
    Drupal.TBWall.masonry_container.find('.grid.tb-wall-single-style').css({
      width: column_width + "px"
    });
    Drupal.TBWall.masonry_container.find('.grid.tb-wall-double-style').css({
      width: (column_width * 2) + "px"
    });
    Drupal.TBWall.masonry_container.find('.grid.tb-wall-triple-style').css({
      width: (column_width * 3) + "px"
    });
    Drupal.TBWall.masonry_container.data('basewidth', column_width);
    return column_width;
  }

  Drupal.TBWall.updateMasonryMainWidth = function(reload) {
    if(Drupal.TBWall.masonry_container) {
      Drupal.TBWall.updateMainColumnWidth();
      if(reload) {
        window.setTimeout(function() {
          Drupal.TBWall.masonry_container.masonry('reload');
        }, 100);
      }
    }
  }

  Drupal.TBWall.updateSidebarColumnWidth = function() {
    Drupal.TBWall.masonry_sidebar_container.addClass('masonry-reload');
    var container_width = Drupal.TBWall.masonry_sidebar_container.width();
    var number_column = Math.round(container_width / Drupal.TBWall.sidebarColumnWidth);
    var column_width = Math.floor(container_width / number_column);
    Drupal.TBWall.masonry_sidebar_container.find('.block, .block.grid-single').css({
      width: column_width + "px"
    });
    Drupal.TBWall.masonry_sidebar_container.find('.block.grid-double').css({
      width: (column_width * 2) + "px"
    });
    Drupal.TBWall.masonry_sidebar_container.find('.block.grid-triple').css({
      width: (column_width * 3) + "px"
    });
    Drupal.TBWall.masonry_sidebar_container.data('basewidth', column_width);
    return column_width;
  }

  Drupal.TBWall.updateMasonrySidebarWidth = function(reload) {
	if(Drupal.TBWall.masonry_sidebar_container) {
	  Drupal.TBWall.updateSidebarColumnWidth();
      if(reload) {
        window.setTimeout(function() {
          Drupal.TBWall.masonry_sidebar_container.masonry('reload');
        }, 20);
      }
	}
  }

  Drupal.TBWall.initMasonry = function() {
    var $container = $('#block-system-main .view .view-content .views-view-grid');
    if($container.length) {
      Drupal.TBWall.masonry_container = $container;
      Drupal.TBWall.masonry_container.addClass('masonry-reload');
      options = {
        itemSelector: '.views-col',
        isResizable: false,
        isAnimated: false,
        columnWidth: function() {
          Drupal.TBWall.masonry_container.removeClass('masonry-reload');
    	  var basewidth = Drupal.TBWall.masonry_container.data('basewidth');
    	  if(basewidth == undefined) {
    		return Drupal.TBWall.updateMainColumnWidth();
    	  }
    	  else {
            return basewidth;
    	  }
        }
      };
      Drupal.TBWall.masonry_container.masonry(options);
      Drupal.TBWall.updateMasonryMainWidth(true);
    }
    var $container = $('#sidebar-first-wrapper .region-sidebar-first');
    if($container.length) {
      Drupal.TBWall.masonry_sidebar_container = $container;
      Drupal.TBWall.masonry_sidebar_container.addClass('masonry-reload');
      options = {
        itemSelector: '.block',
        isResizable: false,
        isAnimated: false,
        columnWidth: function() {
          Drupal.TBWall.masonry_sidebar_container.removeClass('masonry-reload');
          var basewidth = Drupal.TBWall.masonry_sidebar_container.data('basewidth');
          if(basewidth == undefined) {
            return Drupal.TBWall.updateSidebarColumnWidth();
          }
      	  else {
            return basewidth;
      	  }
        }
      }
      Drupal.TBWall.masonry_sidebar_container.masonry(options);
      Drupal.TBWall.updateMasonrySidebarWidth(true);
    }
  };

  Drupal.TBWall.mobilePopup = function() {
    var windowWidth = window.innerWidth ? window.innerWidth : $(window).width();
    if ((Drupal.TBWall.currentWidth == -1) ||
       (windowWidth - Drupal.TBWall.supportedScreens[2]) *
       (Drupal.TBWall.currentWidth - Drupal.TBWall.supportedScreens[2]) < 0) {
      if (windowWidth < Drupal.TBWall.supportedScreens[2]) {
        $('#main-wrapper .colorbox-load.init-colorbox-load-processed.cboxElement').each(function() {
          str = $(this).attr('href');
          parts = str.split("?");
          $(this).removeClass('colorbox-load')
            .removeClass('init-colorbox-load-processed')
            .removeClass('cboxElement')
            .addClass('tb-wall-colorbox-iframe')
            .attr('href', parts[0]);
        });
      }
      else {
    	$('#main-wrapper .tb-wall-colorbox-iframe').each(function() {
          $(this).addClass('colorbox-load')
          .addClass('init-colorbox-load-processed')
          .addClass('cboxElement')
          .removeClass('tb-wall-colorbox-iframe')
          .attr('href', $(this).attr('href') + "?tb_wall_iframe=1&width=550&height=600&iframe=true");
    	})
      }
    }
  }

  Drupal.TBWall.iScrollPopupInit = function() {
    el = $('body.tb-wall-popup-iframe #block-system-main .block-inner');
    if(el.length && !Drupal.TBWall.IE8) {
        el.css({height: $(window).height()});
        Drupal.TBWall.popupIScroll = new iScroll(el[0], {vScroll: true, hScroll: false, vScrollbar: true, hScrollbar: false, scrollbarClass: 'myScrollbar', useTransform: false});
    }
  }

  Drupal.TBWall.iScrollInit = function() {
    if($("#menu-left-inner").length) {
      Drupal.TBWall.sidebarIScroll = new iScroll('menu-left-inner', {
        vScroll: true,
        hScroll: false,
        vScrollbar: true,
        hScrollbar: false,
        scrollbarClass: 'myScrollbar',
        useTransform: false
      });
    }
  }

  Drupal.TBWall.initLazyload = function() {
    $('img[data-src]').each(function() {
      if(!$(this).hasClass('loading-icon')) {
        $(this).addClass('loading-icon').parent().append('<img class="lazyloader-icon" style="position: absolute;" src="' + Drupal.TBWall.lazyload_icon + '"/>');
      }
    });
  }

  Drupal.TBWall.loadActualImages = function(){
    if(Drupal.TBWall.lazyloadFinished) {
      images = $('img[data-src]');
      images.each(function(){
        if (Drupal.TBWall.windowView(this) && $(this).attr('data-src')){
          Drupal.TBWall.loadImage(this);
          $(this).fadeIn('slow');
        }
      });
    }
  };

  Drupal.TBWall.windowView = function(image){
    var windowHeight = $(window).height(),
    windowWidth  = $(window).width(),
    windowBottom = windowHeight + $(window).scrollTop(),
    imageTop     = $(image).offset().top;
    return windowBottom >= imageTop;
  };

  Drupal.TBWall.loadImage = function(image){
    var img = document.createElement('img');
    var src = $(image).attr('data-src');
    $(image).removeAttr('data-src')
    $(img).attr('src', src);
    $(img).imagesLoaded(function(){
      $(image).attr('src', src);
      $(image).removeClass('loading-icon');
      $(image).parent().find('img.lazyloader-icon').remove();
    });
  };

  Drupal.TBWall.updateScrollSize = function() {
    var left_menu = $('#menu-left-wrapper');
    if (left_menu.length) {
      var window_height = window.innerHeight ? window.innerHeight : $(window).height();
      var top = left_menu.offset().top;
      left_menu.css({
        'height': window_height - top
      });
      if(Drupal.TBWall.sidebarIScroll && !Drupal.TBWall.IE8) {
        Drupal.TBWall.sidebarIScroll.refresh();
        Drupal.TBWall.sidebarIScroll.scrollTo(0, 0, 300);
      }
      else if(Drupal.TBWall.IE8){
        $('#menu-left-inner').mCustomScrollbar("update");
        $('#menu-left-inner').mCustomScrollbar("scrollTo", 0);
      }
    }
  }

  Drupal.TBWall.eventStopPropagation = function(event) {
    if (event.stopPropagation) {
      event.stopPropagation();
    }
    else if (window.event) {
      window.event.cancelBubble = true;
    }
  }

  Drupal.TBWall.updateResponsiveMenu = function(){
    var windowWidth = window.innerWidth ? window.innerWidth : $(window).width();
    if ((Drupal.TBWall.currentWidth == -1) ||
       (windowWidth - Drupal.TBWall.supportedScreens[4]) *
       (Drupal.TBWall.currentWidth - Drupal.TBWall.supportedScreens[4]) < 0) {
      if(windowWidth < Drupal.TBWall.supportedScreens[4]){
        $('#menu-bar-wrapper').css({display: 'none'});
        $('#menu-left-wrapper').css({display: 'none'});
        $('#header .responsive-menu-button').show();
      }
      else{
/*        $('#header .responsive-menu-button').hide(); */
        $('#menu-bar-wrapper').css({display: ''});
        $('#menu-left-wrapper').css({display: ''});
      }
    }
  }

  Drupal.TBWall.btnToTop = function(){
    if($(window).scrollTop()) {
      $('#button-btt').fadeIn(1000);
    }
    else {
      $('#button-btt').fadeOut(1000);
    }
  }

  Drupal.TBWall.initResponsiveMenu = function() {
      Drupal.TBWall.updateResponsiveMenu();
      $('#header .tb-left-menu-button').click(function(e){
        var target = $('#menu-left-wrapper');
        target.toggleClass('open');
        target.css({display: target.css('display') != 'none' ? 'none' : 'block'});
        $('#menu-bar-wrapper').css({display: 'none'});
        $('#header-wrapper').css({position: 'fixed'});
        Drupal.TBWall.eventStopPropagation(e);
        Drupal.TBWall.updateScrollSize();
      });
      $('#header .tb-main-menu-button').click(function(e){
        var target = $('#menu-bar-wrapper');
        $('#menu-left-wrapper').css({display: 'none'});
        if(target.css('display') == 'none') {
          window.scrollTo(0, 0);
          target.css({display: 'block'});
          $('#header-wrapper').css({position: 'relative'});
        }
        else {
          target.css({display: 'none'});
          $('#header-wrapper').css({position: 'fixed'});
        }
        Drupal.TBWall.eventStopPropagation(e);
      });
  };
  Drupal.TBWall.initScroll = function() {
    $("#menu-left-inner").mCustomScrollbar({
        set_width:false,
        set_height: '100%',
        horizontalScroll:false,
        scrollInertia:550,
        scrollEasing:"easeOutCirc",
        mouseWheel:"auto",
        autoDraggerLength:true,
        scrollButtons:{
          enable:false,
          scrollType:"continuous",
          scrollSpeed:20,
          scrollAmount:40
        },
        advanced:{
          updateOnBrowserResize:true,
          updateOnContentResize:false,
          autoExpandHorizontalScroll:false
        },
        callbacks:{
          onScroll:function(){},
          onTotalScroll:function(){},
          onTotalScrollOffset:0
        }
      });
  };

  Drupal.TBWall.addEscaparateItems = function(items_selector) {
      var url = "/escaparate?oasync=1";
      if (config.LANGUAGE !== "es") {
          url = "/"+config.LANGUAGE+"/escaparate?oasync=1";
      }
      $.ajax({
          url: url
        })
      .done(function( data ) {

        $('.field-name-field-sello-calidad-auto .field-items .field-item').each(function() {
            if ($(this).is(':empty')) {
              $(this).parent().parent().remove();
            }
        });

        $(data).find('.grid.views-col').each(function(){

          var step = 0;
          if ($(items_selector).length > 20) {
              step = $(items_selector).length - 20;
          }

		  var peso = $(this).find('.grouping-title');
		  peso.hide();

          var pos = Math.floor(Math.random()*($(items_selector).length-step)+step);   //Math.floor(Math.random() * $(items_selector).length) + step;
          console.log("loading in pos: "+pos+"  maximum of: "+$(items_selector).length);
          $('.views-view-grid > .grid.views-col:eq('+pos+')').after(this);

		  if ($(items_selector).length < 40) {
			  if ((peso.text() != "") && (peso.text() < 10)) {
				var resto = parseInt($(items_selector).length / 20) - 1;
				var posicion = 20 * resto;
				var pos = eval(posicion + parseInt(peso.text()));
				$(".views-view-grid").prepend($(this));
			  }
		  }
        });

        if(Drupal.TBWall.masonry_container) {
          Drupal.TBWall.masonry_container.masonry('reload');
        }
        $(items_selector).imagesLoaded(function() {
          if(Drupal.TBWall.masonry_container) {
            Drupal.TBWall.updateMasonryMainWidth();
            Drupal.TBWall.masonry_container.masonry( 'reload', function() {
              Drupal.TBWall.lazyloadFinished = false;
              Drupal.TBWall.initLazyload();
              window.setTimeout(function() {
                Drupal.TBWall.lazyloadFinished = true;
                Drupal.TBWall.loadActualImages();
              }, 100);


            });
          }
        });

      });
  }


  // THEMING PÁGINA QUIENES SOMOS
  Drupal.TBWall.quienesSomos = function() {
	if ($('.view-quienes-somos').length) {
		  // CREAMOS APARTADOS 'FOTOS' Y 'TEXTOS'
		  $('.view-quienes-somos .view-content').wrap( "<div class='quienes-fotos'></div>" );
		  $('<div class="quienes-textos"></div>').insertAfter( '.view-quienes-somos .quienes-fotos' );		  
		  $('.view-quienes-somos .views-fieldset').appendTo('.view-quienes-somos .quienes-textos');

		  var foto = $('.view-quienes-somos .quienes-fotos .views-field-field-image');
		  var numFoto = 1;
		  var mostrado = 1;

		  // ESCONDEMOS TODOS LOS TEXTOS, MENOS EL PRIMERO
		  $('.view-quienes-somos .quienes-textos .views-fieldset').hide();
		  $('.view-quienes-somos .quienes-textos .views-fieldset.fieldset-' + numFoto).show();

		  // MOSTRAMOS TODAS LAS FOTOS CON TRANSPARENCIA, MENOS LA PRIMERA
		  foto.each(function() {
			var nuevo = $(this).find('.field-content').children().attr('class').slice(5,6);
			if (nuevo == numFoto) 	cambiarFotoEquipo($(this),'data-src','2');
			else					cambiarFotoEquipo($(this),'data-src','1');
		  });

		  // CUANDO PASAMOS POR ENCIMA DE UNA FOTO, CAMBIAMOS LA FOTO Y EL TEXTO
		  foto.hover(function() {
			foto.each(function() {
				cambiarFotoEquipo($(this),'src','1');
		    });
			cambiarFotoEquipo($(this),'src','2');

			var nueva = $(this).find('.field-content').children().attr('class').slice(5,6);

			$('.view-quienes-somos .quienes-textos .fieldset-' + mostrado).hide();
			$('.view-quienes-somos .quienes-textos .fieldset-' + nueva).show();
			mostrado = nueva;
		  });
      }
   }

	function cambiarFotoEquipo(fotito,source,num) {
		var url_1 = fotito.find('img').attr(source);
		var url_2 = url_1.substr(0, url_1.length - 5) + num + ".png";

		fotito.find('img').attr(source, url_2);
		if (num == "1")	trans = 0.6;
		else			trans = 1;
		fotito.css({ opacity: trans });
	}


	// DEVUELVE EL NOMBRE DEL ÁMBITO QUE TIENE COMO CLASE
	function getClassAmbito(element) {
		var classes = $(element).attr('class').split(' ');					
		for (var i=0; i<classes.length; i++) {
			if (classes[i].slice(0,22) === 'form-item-edit-ambito-') {
				ambito = classes[i].slice(22,classes[i].length);
				ambitoName = ambito_taxonomy_name(ambito);
				return ambitoName;
				break;
			}
			if (classes[i].slice(0,11) === 'subambitos-') {
				ambitoName = classes[i].slice(11,classes[i].length);
				return ambitoName;
				break;
			}
		}
	}

	// COMPRUEBA SI TIENE ALGUN SUBÁMBITO HIJO CHEQUEADO
	function hasCheckedChildren(item) {
		if (item.find('input[type="checkbox"]:checked').length > 0) {
			return true;
		}
		return false;
	}


    function bindFilters() {

        /* Know height of filter form*/
/*      $initial = $('#views-exposed-form-bastian-theme-of-front-page-2-page').height();
        $("#edit-ambito-wrapper ul.bef-tree > ul > li > div.form-type-bef-checkbox").click(function(){
            var visible = $(this).parent('li').hasClass('visible'),
                checked = $(this).hasClass('ui-state-active'),
                isCheched = $(this).hasClass('checked');

            $("#edit-ambito-wrapper ul.bef-tree > ul > li").removeClass('visible');
            if (!visible) {
                $(this).parent().addClass('visible');
            }
            var maxim = 0;
            $.each($('#edit-ambito-wrapper ul.bef-tree > ul > li.visible'), function(){
                if($($( this ).find( 'ul.bef-tree-depth-1' )).is(':visible')&& maxim < $($(this).find('ul.bef-tree-depth-1' )).height()){
                    maxim = $($(this).find('ul.bef-tree-depth-1' )).height();
                }
            });

            $('#views-exposed-form-bastian-theme-of-front-page-2-page').height($initial + maxim);
			
            if (maxim > 119) {
                $('#edit-ambito-wrapper ul.bef-tree-child.bef-tree-depth-1').attr('style', 'overflow-y: scroll !important');
                $('#edit-ambito-wrapper ul.bef-tree-child.bef-tree-depth-1 > li').css('width', '48%');
            } else {
                $('#edit-ambito-wrapper ul.bef-tree-child.bef-tree-depth-1').attr('style', 'overflow-y: hidden !important');
                $('#edit-ambito-wrapper ul.bef-tree-child.bef-tree-depth-1 > li').css('width', '50%');
            }
            $('#edit-ambito-wrapper .bef-tree-depth-0 > li > .form-type-bef-checkbox').removeClass("ui-state-active");
            $(this).addClass("ui-state-active");
            $.each($('#edit-ambito-wrapper .bef-tree-depth-0 > li > .form-type-bef-checkbox'), function(e){
               if ($(this).parent().find('input[type="checkbox"]:checked').length > 0) {
                  $(this).addClass('ui-state-active');
              }
            });
        });
        // INIT
        $('#edit-ambito-wrapper .form-type-bef-checkbox').removeClass("ui-state-active");
        $.each($('#edit-ambito-wrapper .bef-tree-depth-0 > li > .form-type-bef-checkbox'), function(e){
           if ($(this).parent().find('input[type="checkbox"]:checked').length > 0) {
               $(this).addClass('ui-state-active');
           }
        });

        $.each($('#edit-ambito-wrapper .bef-tree-depth-0 > li > .form-type-bef-checkbox'), function(e){
            $(this).siblings('ul.bef-tree-child').prepend('<li class="select-all"><div class="form-item"><label>Seleccionar todo el ámbito</label></li>');
        });
        $('.select-all').click(function(e){
            e.preventDefault();
            if (!$(this).hasClass('checked')) {
                $(this).find('input[type="checkbox"]').attr('checked', true);
                $(this).parent().find('input[type="checkbox"]').attr('checked', true);
                $(this).addClass('checked');
            } else {
                $(this).find('input[type="checkbox"]').attr('checked', false);
                $(this).parent().find('input[type="checkbox"]').attr('checked', false);
                $(this).removeClass('checked');
            }
        });

        $.each('#edit-edad-wrapper .bef-checkboxes > .form-type-bef-checkbox.highlight', function(){
            var input = $(this).find("input");
            if($(input).is(':checked')){
                $(this).parent().addClass("highlight");
            }
            else {
                $(this).parent().removeClass("highlight");
            }
        });
        $('#edit-edad-wrapper .form-type-bef-checkbox, #edit-idioma-wrapper .form-type-bef-checkbox, #edit-productos-wrapper .form-type-bef-checkbox,  #edit-otros-wrapper .form-type-bef-checkbox,  #edit-recurso-wrapper .form-type-bef-checkbox').click(function()
        {
            var input = $(this).find("input");
            if($(input).is(':checked')){
                $(input).attr('checked', false);
                $(this).removeClass("highlight");

            }
            else {
                $(this).addClass("highlight");
                $(input).attr('checked', true);
            }
        });


        var deselectChaeckboxes = "<div class='filter-deselect-checkboxes'> <span class='des'> " + locale.DESELECCIONAR[config.LANGUAGE] + " </span></div>"
        $('.views-exposed-form > .views-exposed-widgets').append(deselectChaeckboxes);
        var hideFilters = "<div class='hide-filters'> </div>"
        $('.views-exposed-form > .views-exposed-widgets').append(hideFilters);

        /* SUB ambitos labels *
        $.each($('.views-exposed-form .bef-tree-depth-0 > .ui-accordion ul.bef-tree-child > li'), function(el) {
            var active = true;
            $.each($(this).find('input[type="checkbox"]'), function(i){
                if (!$(this).attr('checked')) {
                    active = false;
                }
            })
            if (active) {
                $(this).parent().siblings(".form-type-bef-checkbox").addClass('active');
            }
        });


        /* Only one ambit visible*
        $(".hide-filters").click(function(){
            $('div.block-inner > h2.block-title').click();
        });


        $(".filter-deselect-checkboxes span").click(function(e){
                e.prenventDefault;
                $('#views-exposed-form-bastian-theme-of-front-page-2-page input').not(".filter-deselect-checkboxes input").attr('checked', false);
                $(".form-type-bef-checkbox").removeClass("highlight");
                /* AMBITO*

                $(".form-type-bef-checkbox.icono-ambito").removeClass("ui-state-active");
                $(".form-type-bef-checkbox.icono-ambito").removeClass("checked");
                $(".form-type-bef-checkbox.icono-ambito").removeClass("visible");
                $(".form-type-bef-checkbox.icono-ambito").addClass("ui-state-default");

                $(".form-type-bef-checkbox.icono-ambito").removeClass("ui-state-active");
                $(".form-type-bef-checkbox.icono-ambito").addClass("ui-state-default");
                $("ul.bef-tree-child.bef-tree-depth-1").removeClass("ui-accordion-content-active");
                $("ul.bef-tree-child.bef-tree-depth-1").hide();
                return false;
        }); 					*/


		// CREAMOS EL ÁRBOL DE ÁMBITOS Y SUBÁMBITOS
		$('#edit-ambito-wrapper .form-item-Ambito .form-item').each(function() {
			texto  = $(this).find('label').html();
			guio1 = texto.charAt(0);
			guio2 = texto.charAt(1);
			if (guio1 == '-') {
				if (guio2 == '-') {
					$(this).addClass('item-child-2');
					$(this).find('label').html(texto.substr(2));
				}
				else {
					$(this).addClass('item-child-1');
					$(this).find('label').html(texto.substr(1));
				}
				$(this).hide();
			}
			else {
				$(this).addClass('item-child-0');

				$(this).filter(function () {
					ambitoName = getClassAmbito($(this));
					$(this).addClass('icono-ambito');
					$(this).addClass('icono-ambito-' + ambitoName);
					$(this).append('<div class="subambitos subambitos-' + ambitoName + '"></div>');
				});
			}
		});

		// AGRUPAMOS LOS SUBÁMBITOS DENTRO DE CADA ÁMBITO
        $("#edit-ambito-wrapper .form-checkboxes .form-type-bef-checkbox").each(function(){
			if ($(this).hasClass('item-child-0')) {
				$(this).find('.subambitos').hide();
			}
			else {
				$(this).css('display','inherit');
				$(this).appendTo($(this).prev('.item-child-0').find('.subambitos'));
			}
		});

		// AGRUPAMOS LOS SUBÁMBITOS DENTRO DE CADA ÁMBITO
        $("#edit-ambito-wrapper .form-type-bef-checkbox .subambitos").each(function(){
			$('#edit-ambito-wrapper .bef-checkboxes').append($(this));

			if (hasCheckedChildren($(this))) {
				ambitoName = getClassAmbito($(this));
				$("#edit-ambito-wrapper .form-checkboxes .form-type-bef-checkbox.icono-ambito-" + ambitoName).addClass('visible');
			}
		});





		// MOSTRAMOS O ESCONDEMOS LOS SUBÁMBITOS CUANDO SE HACE CLICK EN CADA ÁMBITO
		var $alturaGlobal = $('#views-exposed-form-bastian-theme-of-front-page-2-page').height();

        $("#edit-ambito-wrapper .form-checkboxes .form-type-bef-checkbox.item-child-0").click(function(){
            var visible = $(this).hasClass('visible'),
                checked = $(this).hasClass('ui-state-active'),
				ambitoName = getClassAmbito($(this));

			var $subambitos = $('#edit-ambito-wrapper .bef-checkboxes .subambitos-' + ambitoName);

			$subambitos.resize(function() {
				$(this).css('height','auto');
			});

			// CERRAMOS OTRO SUBÁMBITO ABIERTO, SI LO HAY
			$hayAbierto = false;
			$altoAntiguo = 0;
			$("#edit-ambito-wrapper .form-checkboxes .form-type-bef-checkbox.item-child-0").not($(this)).each(function(){
				if ($(this).hasClass('visible')) {
					$hayAbierto = true;

					ambitoName = getClassAmbito($(this));
					$altoAntiguo = $('#edit-ambito-wrapper .bef-checkboxes .subambitos-' + ambitoName).height();
					$('#edit-ambito-wrapper .bef-checkboxes .subambitos-' + ambitoName).hide();

					if (!hasCheckedChildren($('#edit-ambito-wrapper .bef-checkboxes .subambitos-' + ambitoName))) {
						$(this).removeClass('visible');
					}
				}
			});

			// MOSTRAMOS O ESCONDEMOS SUBÁMBITOS
			if (visible) {
				$(this).removeClass('visible');
				$subambitos.hide();
				visible = false;
			}
			else {
				$(this).addClass('visible');
				$subambitos.show();
				visible = true;
			}

			// AUMENTAMOS ALTURA DEL BLOQUE PARA MOSTRAR SUBÁMBITOS
            var maxim1 = 0;

			if (visible) {
				maxim1 = maxim2 = $subambitos.height();
			}
			else {
				maxim2 = -($subambitos.height());
			}
	
			maxim3 = 0;		
			if ($hayAbierto)
				maxim3 = $altoAntiguo;

		
			var $alturaCheckbox = $('#views-exposed-form-bastian-theme-of-front-page-2-page #edit-ambito-wrapper .bef-checkboxes').height();
            $('#views-exposed-form-bastian-theme-of-front-page-2-page').height($alturaGlobal + maxim1);
		

			// ACTIVAMOS LOS ÁMBITOS QUE TIENEN ALGUN SUBÁMBITO SELECCIONADO
			$("#edit-ambito-wrapper .form-checkboxes .form-type-bef-checkbox.item-child-0").each(function() {
				if ($(this).find('input[type="checkbox"]:checked').length > 0)
					$(this).addClass('ui-state-active');
				else
					$(this).removeClass('ui-state-active');
			});

		});


		// CONTROL DE CHEQUEO DE LOS FILTROS (CHECKBOX)
        $('#edit-edad-wrapper .form-type-bef-checkbox, #edit-idioma-wrapper .form-type-bef-checkbox, #edit-productos-wrapper .form-type-bef-checkbox,  #edit-otros-wrapper .form-type-bef-checkbox,  #edit-recurso-wrapper .form-type-bef-checkbox').click(function()
        {
            var input = $(this).find("input");
            if($(input).is(':checked')){
                $(input).attr('checked', false);
                $(this).removeClass("highlight");

            }
            else {
                $(this).addClass("highlight");
                $(input).attr('checked', true);
            }
        });


    }
    function AmbitHasSomeChecked(element) {
        var active = false;
        $.each($(element), function(el) {
            $.each($(this).find('input[type="checkbox"]'), function(i){
                if ($(this).attr('checked')) {
                    active = true;
                }
            })
        });
        return active;
    }

  $(document).ready(function(){

    config.LANGUAGE = $('html').attr('lang');
    $.each($('.menuparent.nolink'), function(){
        if ($(this).text() == 'Anónimo') {
            $(this).text('Log in');
        }
    });
    /*
    $('#block-block-24 a').click(function(e){
       if ($(window).width() > 900) {
        e.preventDefault();
        $.colorbox({width:"800px", height:"540px", iframe:false, href:"/user/login?oasync=1"});
       } 
    });
	var getLocation = function(href) {
        var l = document.createElement("a");
        l.href = href;
        return l;
    };
	$('.oasis form').submit(function(e) {
	    e.preventDefault();
	    $('body').css('cursor', 'wait');
	    $.ajax({
         type: "POST",
          url: getLocation($(this).attr('action')).pathname.split('?')[0],
          data: $(this).serialize(),
          success: function(resp) {
              console.log(resp);
              $('body').css('cursor', 'auto');
             parent.jQuery.colorbox.close();
             window.parent.location.reload(true);
           }
        });
	});
    */
   $.each($('.cart-review tbody td.products'), function(el){
       if ($(this).find('a').length <= 0) {
           $(this).closest('tr').remove();
       }
   });
   $.each($('a.facebook-action-connect'), function(el) {
     $(this).attr('href', $(this).attr('href').replace("redirect_uri=https", "redirect_uri=http"));  
   });
   if ($('div.messages.status .uc-price').length > 0) {
       $('div.messages.status').remove();
   }
   function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) 
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) 
        {
            return decodeURIComponent(sParameterName[1]).split('+').join(' ');
        }
    }
   }
   if ($('.page-search-page').length > 0) {
       var value = getUrlParameter('keys');
       if (value !== '') {
           $('#header #search-block-form .form-text').val(value);
       } else {
           value = getUrlParameter('key');
           $('#header #search-block-form .form-text').val(value);
       }
   }
    $('.uc-cart-checkout-form .fieldset.form-wrapper#cart-pane').after('<fieldset class="fieldset form-wrapper subtotal"><div class="inner"></div></fieldset>');
    $('.fieldset.form-wrapper.subtotal .inner').append($('.uc-cart-checkout-form #cart-pane td.subtotal').html());
    
     
    
       
    if ($('.field-name-field-tipo-de-producto .field-items .tipo-C').length > 0) {
      $('#node_otros_productos_full_group_tags h3 .field-group-format-toggler a.field-group-format-title').text(locale.ETIQUETES[config.LANGUAGE]);
    }

    $('.field-name-field-tipo-de-contenido .field-items .tipo-product_kit').attr('href', 'http://www.loscuentosdebastian.com/?Productos[0]=product_kit');
    $('.field-name-field-tipo-de-contenido .field-items .tipo-product').attr('href', 'http://www.loscuentosdebastian.com/?Productos[0]=product');
    $('.field-name-field-tipo-de-producto .field-items .tipo-J').attr('href', 'http://www.loscuentosdebastian.com/?Otros[0]=J');
    $('.field-name-field-tipo-de-producto .field-items .tipo-C').attr('href', 'http://www.loscuentosdebastian.com/?Otros[0]=C');
    $('.field-name-field-tipo-de-producto .field-items .tipo-S').attr('href', 'http://www.loscuentosdebastian.com/?Otros[0]=S');
    $('.field-name-field-tipo-de-recurso .field-items .tipo-L').attr('href', 'http://www.loscuentosdebastian.com/?Recurso[0]=L');
    $('.field-name-field-tipo-de-recurso .field-items .tipo-R').attr('href', 'http://www.loscuentosdebastian.com/?Recurso[0]=R');
    $('.field-name-field-tipo-de-recurso .field-items .tipo-H').attr('href', 'http://www.loscuentosdebastian.com/?Recurso[0]=H');
    $('.field-name-field-tipo-de-recurso .field-items .tipo-T').attr('href', 'http://www.loscuentosdebastian.com/?Recurso[0]=T');
    $('.field-name-field-tipo-de-recurso .field-items .tipo-A').attr('href', 'http://www.loscuentosdebastian.com/?Recurso[0]=A');
    $('.field-name-field-tipo-de-recurso .field-items .tipo-C').attr('href', 'http://www.loscuentosdebastian.com/?Recurso[0]=C');

    if (config.LANGUAGE === 'ca') {
//    $('.remember-dto').css('background', 'url("../images/topo_cat.png") 0 0 no-repeat');
      $('.views-exposed-form .bef-checkboxes .form-item-edit-edad-10').addClass('ca');
      $('.views-exposed-form .bef-checkboxes .form-item-edit-edad-10:hover , .views-exposed-form .bef-checkboxes .form-item-edit-edad-10.highlight').addClass('ca');
      $('a[href="/ca/edad/adulto"] img').replaceWith('<img typeof="foaf:Image" src="http://www.loscuentosdebastian.com/sites/default/files/adult-02-red.png" width="31" height="26" alt="" class="">')
    }

    $('.field-name-field-disponible a, .field-name-field-desenvolupador a, .field-name-field-pru-valo-en a, .field-name-field-elaborado a').click(function(e) {
      e.preventDefault();
      window.open($(this).attr('href'));
    });
    $('label.option').each(function(){
       if ($(this).text().replace(/\s+/g, '') == "Catalán" || $(this).text().replace(/\s+/g, '') == "Català") {
           $(this).addClass('icon-ca');
       }
       if ($(this).text().replace(/\s+/g, '') == "Castellano" || $(this).text().replace(/\s+/g, '') == "Castellà") {
           $(this).addClass('icon-es');
       }
       if ($(this).text().replace(/\s+/g, '') == "Inglés" || $(this).text().replace(/\s+/g, '') == "Anglès ") {
           $(this).addClass('icon-en');
       }
    });
    var fboauth = $("#block-fboauth-login").clone();
    var fboauth2 = $("#block-fboauth-login").clone();
    $("#block-fboauth-login").remove();
    $('form#user-login').append('<p>'+locale.FB_PRE[config.LANGUAGE]+'</p>');
    $('form#user-login').append(fboauth.find('a.facebook-action-connect').html('Entra con <span class="icon facebook">f</span>'));
    $('form#user-register-form').append('<p>'+locale.FB_PRE[config.LANGUAGE]+'</p>');
    $('form#user-register-form').append(fboauth2.find('a.facebook-action-connect').html('Regístrate con <span class="icon facebook">f</span>'));
    
//  $('#login-form').after('<div class="login-bastian"><h2>'+locale.BASTIAN_LOGIN[config.LANGUAGE]+'</h2><img src="/sites/default/files/register-bastian.jpg"></div>');

/*  $('.main-menu-full').prepend('<button class="close-menu"></button>');
    $('button.main-menu, button.close-menu').click(function(e){
        e.preventDefault();
        $('.main-menu-full').toggleClass('hidden');
        $('#page').toggleClass('filtre-open');
    });	*/

    $('.mmenu-nav').prepend('<button class="close-menu"></button>');
    $('button.close-menu').click(function(e){
		e.preventDefault();
        $('.mmenu-nav').trigger('close.mm');
    });

    $('.views-exposed-form .bef-checkboxes .form-item-edit-edad-11').hover(function(){
      $(this).css('background', 'url("../images/juvenil_hover.png") no-repeat !important');
    }, function(){
      $(this).removeClass('state-hover');
    });

    if ($('.node-full .group-comprar input[type="radio"]').length > 0) {
      $('#node_product_kit_full_group_add_to_cart form').prepend('<div class="select-language"><label class="select-language-label">'+locale.SELECIDIOMA[config.LANGUAGE]+'</label></div>');
      $('#node_product_full_group_product_right .select-language > label').replaceWith('<label class="select-language-label">'+locale.SELECIDIOMA[config.LANGUAGE]+'</label>');
    }
    if ($('#block-language-switcher-fallback-language-switcher-fallback').offset().left > ($('#main-wrapper').width() + $('#main-wrapper').offset().left - 90)) {
        $('#block-language-switcher-fallback-language-switcher-fallback, #block-block-19').css('right', 'auto');
        $('#block-language-switcher-fallback-language-switcher-fallback').css('left', $('#main-wrapper').width() + $('#main-wrapper').offset().left-90);
        $('#block-block-19').css('left', $('#main-wrapper').width() + $('#main-wrapper').offset().left-150);
    }

    var originalPrice = $('#node_product_full_group_product_right .price-item .uc-price').text().slice(0,-1);
    $('#node_product_full_group_product_right .price-item .uc-price').attr('original-price', originalPrice);


    var pattern = /[0-9]+/g;
    desc = $('.attributes .form-item input[checked=checked]').next().text().slice(0,-1);
    op = desc.indexOf("-");
    if (op != -1) {
      op = "-";
    }
    else {
      op = "+";
    }
    desc = desc.match(pattern);

    if (desc != null) {
      descompt = op + desc[0] + '.' + desc[1];
      descompt = parseFloat(descompt);
      originalPrice = originalPrice.replace(",", ".");
      total = (parseFloat(originalPrice) + descompt).toFixed(2);
      total = total.replace(".", ",");
      $('#node_product_full_group_product_right .price-item .uc-price').html(total + '€');
    }
    
/*    $('#header-wrapper #block-block-25').click(function(e) {
      if ($(window).width() < 1000) {
        if ($('#header-wrapper #block-search-form').is(":visible")) {
          $('#header-wrapper #block-search-form').css('display', 'none');
        }
        else {
          $('#header-wrapper #block-search-form').css('display', 'block');
        }
      }
    });*/


    $('.attributes .form-item input').click(function() {

      var pattern = /[0-9]+/g;
      desc = $(this).next().text().slice(0,-1);
      op = desc.indexOf("-");
      if (op != -1) {
        op = "-";
      }
      else {
        op = "+";
      }
      desc = desc.match(pattern);
      if (desc != null) {
        descompt = op + desc[0] + '.' + desc[1];
        descompt = parseFloat(descompt);
        originalPrice = $('#node_product_full_group_product_right .price-item .uc-price').attr('original-price');
        originalPrice = originalPrice.replace(",", ".");
        total = (parseFloat(originalPrice) + descompt).toFixed(2);
        total = total.replace(".", ",");
        $('#node_product_full_group_product_right .price-item .uc-price').html(total + '€');
      }
      else {
        descompt = parseFloat(0.0);
        originalPrice = $('#node_product_full_group_product_right .price-item .uc-price').attr('original-price');
        originalPrice = originalPrice.replace(",", ".");
        total = (parseFloat(originalPrice) + descompt).toFixed(2);
        total = total.replace(".", ",");
        $('#node_product_full_group_product_right .price-item .uc-price').html(total + '€');
      }
    });

	// DESACTIVEM PEL NOU MENÚ DESPLEGABLE
/*    if (jQuery('body').width() > 550) {
        $('.region.region-header .block-views').css('left', (jQuery('body').width()/2)-(545/2));
    }	*/
	// DESACTIVEM PEL NOU MENÚ DESPLEGABLE



    $('form#search-block-form').submit(function( event ) {
      event.preventDefault();
      var value = $('form#search-block-form .custom-search-box.form-text').val();
      value = value.replace(" ", "+");

	  idioma = "";
      if (config.LANGUAGE !== "es") {
		  idioma = '/'  + config.LANGUAGE;
	  }

      var url = window.location.protocol + '//' + window.location.host + idioma + '/search-page?keys=' + value;
//    var url = window.location.protocol + '//' + window.location.host + '/search-page?keys=' + value;

      window.location.href = url;
    });
    $('.field-name-field-newsletter .form-item').prepend('<label>Newsletter</label>');

    $('.region-header div.block.block-views .block-title').click(function(e){
        e.preventDefault();
        $('#page').toggleClass('filtre-open');
        $(this).siblings('.block-content').slideToggle();
    });

    var votes = $('#node_product_full_group_product_right .field-name-field-rating .fivestar-summary .empty').text();
    var votesFinal = votes.substring(1, votes.length-1) + ' + ' + $('.block-content .comment-wrapper .comment').length + ' ' + locale.OPINION[config.LANGUAGE];
    $('.field-name-field-rating .fivestar-summary .empty').replaceWith(votesFinal);

    var votesTotal = $('#node_product_full_group_product_right .field-name-field-rating .fivestar-summary .total-votes').text();
    var votesTotalFinal = votesTotal.substring(1, votesTotal.length-1) + ' + ' + $('.block-content .comment-wrapper .comment .field-name-comment-body').length + ' ' + locale.OPINION[config.LANGUAGE];

    $('.node-full .group-ambito .field-name-field-ambito ul.term li > .taxonomy-tree-image').each(function(index) {
      intern = $(this).next();
      $(this).find('a').append(intern);
    });

    $('.group-ambito.field-group-div div.term-tree-list > ul.term > li').each(function(index) {
      var color = $(this).find('.taxonomy-tree-image a .taxonomy-tree-text a').css('color');
      $(this).find('ul.term > li > a').css('color', color);

    });


   $.each($('.language-switcher-locale-url a'), function(el){
       if (location.search !== "")
        $(this).attr('href', $(this).attr('href')+location.search);
   });

    $('.field-name-field-sello-calidad-auto .field-items .field-item').each(function() {
      if ($(this).is(':empty')) {
        $(this).parent().parent().remove();
      }
    });

    $("button[data-action='subscribe-block']").click(function(e){
        e.preventDefault();
        var nid = $("ul.prices > li").attr('data-product');
        window.location = "/cart/add/p"+nid+"?destination=cart";
    });
    $("li[data-subs]").click(function(e){
        e.preventDefault();
        $("ul > li[data-subs], ul > li[data-tab]").removeClass('active');
        $("ul.prices > li[data-tab='"+$(this).attr('data-subs')+"']").addClass("active");
        $(this).addClass('active');

    });

/*	if ($("body").hasClass('node-type-recursos') && !$("#page").hasClass('premium')) {
        $(".description-recurso").addClass('trimmed');
        var bodyHeight = $(".description-recurso .field-name-body").height();
        if (bodyHeight > 100) {
            $('.description-recurso.trimmed').css('height', bodyHeight+150);
            $('.description-recurso.trimmed').css('max-height', "100%");
        }
        $(".description-recurso").append('<div class="abs subscribe-link"><a href="/product/registro">'+locale.SUBSCRIBE[config.LANGUAGE]+"</a></div>");
    } */
	/*else {
        $(".description-recurso").addClass('trimmed');
        $(".description-recurso").append('<div class="abs subscribe-link showmore"><a href="'+$("#block-block-16 .block-content > a").attr('href')+'">+ info</a></div>');
    }
    $('.subscribe-link.showmore').click(function(e){
        e.preventDefault();
        $(this).parent().parent().removeClass('trimmed');
        $(this).remove();

    });*/
   
   $('.form-type-uc-quantity input[type="text"]').hide();
   var options = '';
   for (var i = 1; i<11; i++) {
       options = options +'<option value="'+i+'">'+i+'</option>';
   }
   $.each($('.form-type-uc-quantity input[type="text"]'), function(){
      $(this).after('<select class="select-dp">'+options+'</select>');
      $(this).parent().find('.select-dp').val($(this).val()); 
   });
   $(".form-type-uc-quantity .select-dp").change(function(e){
       $(this).parent().find('input[type="text"]').val($(this).val());
   });
   
   var querystring = window.location.search.substring(1);
    if (querystring !== "") {
        $('.field-name-field-imagen-pequena a, .field-name-field-titulo-link a, a.prev-link, a.next-link, a.home-link').each(function(){
            var href = $(this).attr('href');
            if(href) {
                href += (href.match(/\?/) ? '&' : '?') + querystring;
                $(this).attr('href', href);
            }
        });
    }

/* $('#node_product_full_group_add_to_cart').append('<span class="icon remember-dto"></span>');
   $('#node_otros_productos_full_group_add_to_cart').append('<span class="icon remember-dto"></span>');
   $('#node_product_kit_full_group_add_to_cart').append('<span class="icon remember-dto"></span>');			*/

    $('label[for="edit-panes-uc-termsofservice-agreement-checkout-tos-agree-agreed"]').html($('label[for="edit-panes-uc-termsofservice-agreement-checkout-tos-agree-agreed"]').text().replace('anteriores', '<a target="_blank" href="/condiciones-generales-y-politica-de-privacidad">planteados aquí</a>'));
    $('label[for="edit-panes-uc-termsofservice-agreement-checkout-tos-agree-agreed"]').html($('label[for="edit-panes-uc-termsofservice-agreement-checkout-tos-agree-agreed"]').text().replace('anteriors', '<a target="_blank" href="/ca/condicions-generals-i-politica-de-privacitat">plantejats aqui</a>'));
    $('#payment-pane .description').html('<h2>'+locale.TRAMITAR[config.LANGUAGE]+'</h2>');
    $('#payment-details').prepend('<h2>'+locale.TARGETA[config.LANGUAGE]+'*</h2>');
    $('#payment-pane .description').append($('#line-items-div, .form-item-panes-payment-payment-method'));
     $(document).ajaxStop(function() {
         $('label[for="edit-panes-uc-termsofservice-agreement-checkout-tos-agree-agreed"]').html($('label[for="edit-panes-uc-termsofservice-agreement-checkout-tos-agree-agreed"]').text().replace('anteriores', '<a target="_blank" href="/condiciones-generales-y-politica-de-privacidad">planteados aquí</a>'));
        $('#payment-pane .description').prepend('<h2>'+locale.TRAMITAR[config.LANGUAGE]+'</h2>');
        $('#payment-details').prepend('<h2>'+locale.TARGETA[config.LANGUAGE]+'*</h2>');
        $('#payment-pane .description').append($('#line-items-div, .form-item-panes-payment-payment-method'));
     });
    var view_selector    = 'div.view-front-page-2.view-display-id-page';
    var items_selector   = view_selector + ' .grid.views-col';
    if ($(view_selector).length > 0) {
//        if (!$('#page').hasClass('premium')) {
            Drupal.TBWall.addEscaparateItems(items_selector);
//        }
    }
    
    $('#coupon-pane').prepend('<div class="form-item line-items-money"></div>');
    $('#coupon-pane .line-items-money').html($('#payment-pane #line-items-div').clone());
    $('#coupon-pane .line-items-money').append('<div class="total"><div class="inner"><span class="total">Total</span><span class="money">'+$('#coupon-pane .line-items-money .line-item-total .uc-price').text()+'</div></div>');
    if (config.LANGUAGE == 'ca') {
        $.each($('#coupon-pane .line-items-money td.title, td.desc'), function(el){
            if ($(this).text().substring(0, 22) == 'Descuento suscriptores') {
                $(this).text($(this).text().replace('Descuento suscriptores', 'Descompte subscriptors'));
            }
        });
    }
    window.setInterval(function(){
        if ($('#payment-pane #line-items-div').length > 0) {
			if ($('#coupon-pane .line-items-money').length == 0)
				$('#coupon-pane').prepend('<div class="form-item line-items-money"></div>');

            $('#coupon-pane .line-items-money').html($('#payment-pane #line-items-div').eq(0).clone());
            $('#coupon-pane .line-items-money').append('<div class="total"><div class="inner"><span class="total">Total</span><span class="money">'+$('#coupon-pane .line-items-money .line-item-total .uc-price').text()+'</div></div>');
            if (config.LANGUAGE == 'ca') {
                $.each($('#coupon-pane .line-items-money td.title'), function(el){
                    if ($(this).text().substring(0, 22) == 'Descuento suscriptores') {
                        $(this).text($(this).text().replace('Descuento suscriptores', 'Descompte subscriptors'));
                    }
                });
            }
        }
    }, 1000);

    $('table.sticky-table thead tr th:first-child').remove();

    $('table.sticky-table tbody tr').each(function(index) {
      rem = $(this).find('td.remove').html();
      $(this).find('td.desc').append(rem);
      $(this).find('td.remove').remove();
    });

    $('#uc-cart-view-form .form-actions .button.edit-empty, #uc-cart-view-form .form-actions .button.edit-update').remove();
    $('table.sticky-table thead tr th abbr').empty().append(locale.CANTIDAD[config.LANGUAGE]);
    $('table.sticky-table thead tr th abbr').css('margin-left', '50px');
    $('table.sticky-table thead tr th abbr').css('padding-right', '50px');
    

    $('table.sticky-table .subtotal').removeAttr('colspan');
    $('#uc-cart-view-form table.sticky-table .subtotal').before('<td colspan="3"></td>');
    $('.uc-cart-checkout-form table.sticky-table .subtotal').before('<td colspan="2"></td>');
    $('.uc-cart-checkout-form #comments-pane label').hide();
    $('.uc-cart-checkout-form #comments-pane textarea').attr('placeholder', $('.uc-cart-checkout-form #comments-pane label').text());


    $('.views-exposed-form ul.bef-tree li .form-item span').remove();
    $('.views-exposed-form ul.bef-tree li .form-item').each(function(index) {
      $(this).append('<span class="ui-icon ui-icon-triangle-1-e"></span>');
    });
    if (!$("#page").hasClass("premium")) {
//      $('.views-exposed-form #edit-ambito-wrapper .bef-tree-depth-0 > .ui-accordion > ul.bef-tree-child').remove();
        $('#block-bastian-subscription .prices > li').click(function(e){
          e.preventDefault();
          window.location.href = $(this).find('a').attr('href');
        });
    }


      bindFilters();


      if($('.gallery-thumbs').length > 0) {
          $(".gallery-thumbs").prependTo("div.article.recurso.node");
      }
      if($('.group-comprar.field-group-div #edit-attributes-3 input[checked]').length) {
          $.each($('.group-comprar.field-group-div #edit-attributes-3 input[checked]'), function(){
              $(this).parent('div').addClass("checked");
          });
      }
      $(".group-comprar.field-group-div #edit-attributes-3 > div").unbind();
      $(".group-comprar.field-group-div #edit-attributes-3 > div label").click(function () {
          $(".group-comprar.field-group-div #edit-attributes-3 > div").removeClass("checked");
          if(!$(this).parent().hasClass('checked'))$(this).parent().toggleClass('checked');
		  mostrarPrecioDescuento(true);
      });

      if($('.group-ambito.field-group-div').length) {
          var num = $(' .group-ambito.field-group-div .term-tree-list > ul > li ').length
          var width = 100/num;
          width = width+"%";
          $.each($('.group-ambito.field-group-div .term-tree-list > ul > li'), function(){
              $(this).width(width);
          });
      }
      if($('#edit-comment-body-und-0-value').length) {
          $('#edit-comment-body-und-0-value').attr("placeholder", "Comparte tu opinión");
      }


	// FLAG WISH
	if ($('.flag-outer-wish').length) {
		$('.flag-outer-wish .flag-wrapper').append('<span class="flag-message flag-success-message flag-auto-remove"></span>');
		if ($('.flag-outer-wish img').hasClass('flag-wish-on')) {
			$('.flag-outer-wish .flag-message').html(locale.UNDESEO[config.LANGUAGE]);
			$('.flag-outer-wish .flag-message').addClass('flag-unflagged-message'); 
		}
		else {
			$('.flag-outer-wish .flag-message').html(locale.DESEO[config.LANGUAGE]);
			$('.flag-outer-wish .flag-message').addClass('flag-flagged-message'); 
		}
	}


	// PRODUCTO PERSONALIZADO
	if ($('.attribute-6').length) {
		$('.attribute-6 label:first-child').html(locale.PERSONALIZAR[config.LANGUAGE]);
		
		$('.attribute-6 .form-radios label').each(function() {
				$(this).html($(this).text().replace(',', ''));
		});
	}


	// PRODUCTO DISPONIBLE EN OTROS IDIOMAS
	if ($('.field-name-field-disponible-catalan').length) {
		$('.field-name-field-disponible-catalan a').html("También disponible en catalán");
	}
	if ($('.field-name-field-disponible-castellano').length) {
		$('.field-name-field-disponible-castellano a').html("También disponible en castellano");
	}
	if ($('.field-name-field-disponible-ingles').length) {
		$('.field-name-field-disponible-ingles a').html("También disponible en inglés");
	}

	  Drupal.TBWall.quienesSomos();

  });
  $(window).load(function() {

    var msnry = $('#node_product_full_group_superior').masonry();

    if ($('.node-full.recurso').length > 0) {
        $( window ).bind( 'hashchange', function() {
            var id = location.hash;
            $('.group-galleryformatter .gallery-frame > ul > li.gallery-slide').hide();
            $(id).show();
        });
        window.setTimeout(function(e){

            $('.group-galleryformatter .gallery-frame > ul > li.gallery-slide').eq(0).show();

        }, 300);

    }

      $('.faq-question').click(function() {
        if ($(this).prev().hasClass('ui-icon-triangle-1-e')) {
          $(this).prev().removeClass('ui-icon-triangle-1-e').addClass('ui-icon-triangle-1-s');
        }
        else {
          $(this).prev().removeClass('ui-icon-triangle-1-s').addClass('ui-icon-triangle-1-e');
        }
      });

      if ($('.article').attr('about') === '/product/el-club-de-bastian') {
        if (! $('#edit-ambito-wrapper').hasClass('non-premium')) {
          $('.product-info .uc-price').empty().append(locale.SUBSCRITO[config.LANGUAGE]);
          $('.add-to-cart').empty();
        }
      }



	Drupal.TBWall.mobilePopup();
	Drupal.TBWall.currentWidth = window.innerWidth ? window.innerWidth : $(window).width();
	Drupal.TBWall.toolbar = $('#toolbar').length ? $("#toolbar") : false;
	Drupal.TBWall.btnToTop();
	$('#button-btt').smoothScroll();
	Drupal.TBWall.initResponsiveMenu();
	if (Drupal.TBWall.IE8) {
	  Drupal.TBWall.initScroll();
	  Drupal.TBWall.updateScrollSize();
	} else {
	  Drupal.TBWall.iScrollInit();
	  Drupal.TBWall.updateScrollSize();
	}
	Drupal.TBWall.iScrollPopupInit();
	Drupal.TBWall.initMasonry();
	$("#block-system-main .view .view-content .views-view-grid").imagesLoaded(function() {
	  Drupal.TBWall.initLazyload();
	  if(Drupal.TBWall.masonry_container) {
		Drupal.TBWall.masonry_container.masonry('reload');
	  }
	  window.setTimeout(function() {
		Drupal.TBWall.lazyloadFinished = true
		Drupal.TBWall.loadActualImages();
	  }, 100);
	});
	$("body.tb-wall-popup-iframe").find("a").attr('target', '_parent');
	Drupal.TBWall.ie8Resize();


	// EU COMPLIANCE MESSAGE
	if ($('#sliding-popup').length) {
		$('#sliding-popup .agree-button').html(' X ');
		$('#sliding-popup .find-more-button').hide();
	}


	// AFILIACION
	if ($('#afiliacion-enlace').length) {
		$('#afiliacion-enlace').click(function() {
			if ($('#afiliacion-textarea').css('display') == 'none') {
				$('#afiliacion-textarea').css('display','inherit');
			}
			else {
				$('#afiliacion-textarea').css('display','none');
			}
		});
	}

  });

})(jQuery);


function ambito_taxonomy_name(ambito) {
	switch(ambito) {
		case "204","609": nombre = "personal"; break;
		case "205","613": nombre = "psicomotriz"; break;
		case "206","615": nombre = "emocional"; break;
		case "209","618": nombre = "ambiental"; break;
		case "211","621": nombre = "intelectual"; break;
		case "207","623": nombre = "interpersonal"; break;
		case "208","628": nombre = "socialcivico"; break;
		case "210","631": nombre = "trascendente"; break;
		default:	nombre = "personal";
	}
	return nombre;
}


function mostrarPrecioDescuento($cambio) {
	var priceIni = jQuery('.group-product-right .display-price .uc-price').text().slice(0,-1);
	var descuento = jQuery('.group-product-right .field-name-field-precio-descuento .field-item').text();
	if (descuento != 'no') {
		var price = priceIni.replace(',','.');
		$price_desc = parseFloat(price) - (parseFloat(price) * 0.05);
//		console.log(priceIni,price,parseFloat(price),$price_desc);
		$price_desc = $price_desc.toFixed(2).replace('.', ',');
		if ($cambio) {
			jQuery('.group-product-right .display-price .precio-descuento').remove();
			jQuery('.group-product-right .field-name-field-precio-descuento .field-item').html("€");
		}
		jQuery('.group-product-right .field-name-field-precio-descuento .field-item').prepend($price_desc);
		jQuery('.group-product-right .field-name-field-precio-descuento .field-item').wrapInner("<span class='precio-descuento'></span>");
		jQuery('.group-product-right .field-name-field-precio-descuento .precio-descuento').insertAfter(jQuery('.group-product-right .display-price .uc-price'));
	}
	else {
//		console.log(descuento);
		jQuery('.group-product-right .field-name-field-precio-descuento').hide();
		jQuery('.group-product-right .display-price .uc-price').addClass('precio-descuento');
	}	

}