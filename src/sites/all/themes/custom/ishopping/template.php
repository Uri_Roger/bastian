<?php
/**
 * @file
 * controls load theme.
 */

/**
 * Preprocess theme function to print a single record from a row.
 */
function ishopping_preprocess_views_view_fields(&$vars) {
  if (isset($vars['view']->style_plugin->definition['module']) && $vars['view']->style_plugin->definition['module'] == 'views_slideshow') {
    $fields = $vars['fields'];
    if (count($fields) >= 2) {
      $fields_key = array_keys($fields);
      $fields[$fields_key[1]]->wrapper_prefix = '<div class="slideshow-group-fields-wrapper">' . $fields[$fields_key[1]]->wrapper_prefix;
      $fields[$fields_key[count($fields_key) - 1]]->wrapper_suffix = $fields[$fields_key[count($fields_key) - 1]]->wrapper_suffix . '</div>';
    }
  }
}



/**
 * Override or insert variables into the node template.
 *
 * @param array $vars
 *   An array of variables to pass to the theme template.
 */
function ishopping_preprocess_node(&$variables) {


	// SI EL CAMPO DESCRIPCI�N CONTIENE UN V�DEO, A�ADIMOS EL COLOR GRIS DE FONDO

	drupal_add_js("	
		jQuery(document).ready(function () { 
			var video = jQuery('div.field-name-body object');
			ancho = video.width();
			video.wrap('<div class=\"video-object\">');
			
			jQuery('div.field-name-body .video-object').width(ancho);
		}
	);", "inline");	



	// ESCONDEMOS LA INFORMACI�N DE PRODUCTOS EN STOCK, PARA USUARIOS NORMALES

	if (!in_array("administrator",$variables["user"]->roles) && !in_array("editor",$variables["user"]->roles)) {
			drupal_add_js("	
				jQuery(document).ready(function () { 
					var stock = jQuery('div.stock-product');
					stock.hide();
				}
			);", "inline");	
	}

}




/**
 * Override or insert variables into the page template.
 *
 * @param array $vars
 *   An array of variables to pass to the theme template.
 */
function ishopping_preprocess_page(&$vars) {
	  if ($vars['is_front']) {
		if (theme_get_setting('hide_frontpage_main_content')) {
		  $vars['page']['content']['system_main'] = array();
		}
	  }
	  global $theme_key;
	  $current_skin = theme_get_setting('skin');
	  if (isset($_COOKIE['nucleus_skin'])) {
		$current_skin = $_COOKIE['nucleus_skin'];
	  }
	
	  $vars['page']['show_skins_menu'] = $show_skins_menu = theme_get_setting('show_skins_menu');
	  if ($show_skins_menu) {
		$skins = nucleus_get_predefined_param('skins', array("default" => t("Default Style")));
		$current_skin = theme_get_setting('skin');
		if (isset($_COOKIE['nucleus_skin'])) {
		  $current_skin = $_COOKIE['nucleus_skin'];
		}
		$str = array();
		$str[] = '<div id="change_skin_menu_wrapper" class="change-skin-menu-wrapper wrapper">';
		$str[] = '<div class="container">';
		$str[] = '<ul class="change-skin-menu">';
	
		foreach ($skins as $skin => $skin_title) {
		  $li_class = ($skin == $current_skin ? ($skin . ' active') : $skin);
		  $str[] = '<li class="' . $li_class . '"><a href="#change-skin/' . $skin . '" class="change-skin-button color-' . $skin . '">' . $skin_title . '</a></li>';
		}
		$str[] = '</ul></div></div>';
		$vars['page']['show_skins_menu'] = implode("", $str);
	  }
	  $default_logo = theme_get_setting("default_logo");
	  $toggle_logo = theme_get_setting("toggle_logo");
	  if (!empty($current_skin) && $current_skin != 'default'  && $default_logo && $toggle_logo) {
		$vars['logo'] = file_create_url(drupal_get_path('theme', $theme_key)) . "/skins/" . $current_skin . "/logo.png";
	  }






	// SI LA P�GINA ACTUAL ES LA DE REGISTRO DE NUEVO USUARIO, A�ADIMOS EL COLORBOX AL LINK DE "T�RMINOS Y CONDICIONES"
//	if (in_array("page__user__register",$vars['theme_hook_suggestions'])) {
	if (in_array("page__user",$vars['theme_hook_suggestions']) ||
		in_array("page__legal_accept",$vars['theme_hook_suggestions'])) {

				// MEDIANTE JQUERY, A�ADIMOS EL COLORBOX AL LINK DE "T�RMINOS Y CONDICIONES"
				drupal_add_js("	
					jQuery(document).ready(function () { 
						var link = jQuery('div.form-item-legal-accept a');
						link.colorbox();
						
						jQuery('div.field-name-field-aviso-correo textarea').attr('disabled', 'disabled');
						jQuery('div.field-name-field-aviso-correo .form-type-textarea div').removeClass('resizable');
						jQuery('div.field-name-field-aviso-correo .form-type-textarea div').removeClass('resizable-textarea');
		
					}
				);", "inline");	

	}



	// SI LA P�GINA ACTUAL ES UNA TAXONOM�A DE UN PRODUCTO, MOSTRAMOS ACTIVADO SU MEN� CORRESPONDIENTE (POR EJEMPLO, TAGS --> LIBROS)
	if (in_array("page__node__product",$vars['theme_hook_suggestions'])) {

			// AVERIGUAMOS EL T�RMINO DE TAXONOM�A A LA QUE CORRESPONDE EL NODO		
			$node = node_load(arg(1));
			$term = $node->taxonomy_catalog["und"][0]["tid"];
	
			// MEDIANTE JQUERY, MOSTRAMOS ACTIVADO EL MEN� CORRESPONDIENTE
			drupal_add_js("	
				jQuery(document).ready(function () { 
					jQuery('#block-system-main-menu ul.menu li#" . $term . " a').addClass('active');
				}
			);", "inline");	

	}



	// SI LA P�GINA ACTUAL ES EL FORMULARIO DE PAGO DE LA CESTA, A�ADIMOS LA CLASE "COLORBOX" A LAS CONDICIONES Y POL�TICA DE PRIVACIDAD

	if (in_array("page__cart__checkout",$vars['theme_hook_suggestions'])) {

				// MEDIANTE JQUERY, A�ADIMOS LA CLASE "COLORBOX"
				drupal_add_js("	
					jQuery(document).ready(function () { 
						var link = jQuery('#uc_termsofservice_agreement_checkout-pane li.node-readmore a');
						link.colorbox();
					}
				);", "inline");	
		
		
		
		
				// SI EN LA CESTA HAY UNA SUSCRIPCI�N, MOSTRAMOS EL AVISO DE ALTA DE CUENTA EN PAYPAL
			
				$suscripcion = false;
				$items = uc_cart_get_contents();
				if (!empty($items)) {
					foreach ($items as $item)
						if (isset($item->model)) {
							if ($item->nid == 172) {
								$suscripcion = true;
								break;
							}
						}
				}
				
				// SI HAY SUSCRIPCI�N, ESCONDEMOS EL AVISO DE PAGO CON TARJETA
				if ($suscripcion) {
						drupal_add_js("	
							jQuery(document).ready(function () { 
								jQuery('#webform_nid176-pane').hide();
							}
						);", "inline");	
				}
				// SI NO HAY SUSCRIPCI�N, ESCONDEMOS EL AVISO DE ALTA DE CUENTA EN PAYPAL
				else {
						drupal_add_js("	
							jQuery(document).ready(function () { 
								jQuery('#webform_nid207-pane').hide();
							}
						);", "inline");	
				}


	}




	// SI LA P�GINA CONTIENE ALGUN V�DEO, CAMBIAMOS SU CODIFICACI�N PARA QUE SE VEA CORRECTAMENTE EN TODOS LOS DISPOSITIVOS

/*
	drupal_add_js("	
		jQuery(document).ready(function () { 

			if (jQuery(this).find('#main-content video').length){
				var video = jQuery('#main-content video');

				var destino = video.attr('src');
				var filename= destino.split('/').pop();


				var new_video = '<video width=\"360\" controls preload><source src=\"/sites/default/files/videos/original/' + filename + '\" type=\'video/mp4\'>Your browser does not support the video tag.</video>';


				video.after(new_video);

				var object = jQuery('#main-content object');
				object.hide();
				video.hide();


	        }

		}
	);", "inline");	
*/





/*
	// SI LA P�GINA ACTUAL ES EL FORMULARIO DE REVISI�N FINAL DEL PAGO DE LA CESTA, SI SE MUESTRA ERROR DE PA�S, DESACTIVAMOS EL BOT�N DE SUBMIT
	if (in_array("page__cart__checkout__review",$vars['theme_hook_suggestions'])) {

		// MEDIANTE JQUERY, A�ADIMOS LA CLASE "COLORBOX"
		drupal_add_js("	
			jQuery(document).ready(function () { 
				var mensaje = jQuery('#system-messages-wrapper div.error');
				if (mensaje.length) {
					var button = jQuery('div.form-actions #edit-submit');
					button.hide();
				}
			}
		);", "inline");	
	}
*/
	
	

	// MEDIANTE JQUERY, A�ADIMOS LA CLASE "COLORBOX" A LOS BLOQUES DE "CONDICIONES GENERALES" Y "POL�TICA DE PRIVACIDAD
/*	drupal_add_js("	
		jQuery(document).ready(function () { 
			var link = jQuery('#block-block-7--2 a');
			link.colorbox();

			var link = jQuery('#block-block-8--2 a');
			link.colorbox();
		}
	);", "inline");	
*/



	// SI LA P�GINA ACTUAL ES EL LISTADO DE REVISTAS, A�ADIMOS EL DESTINO EN UNA NUEVA VENTANA PARA LOS ENLACES DE LAS REVISTAS
	if (in_array("page__node__28",$vars['theme_hook_suggestions'])) {
	
			// MEDIANTE JQUERY, A�ADIMOS EL ATRIBUTO "TARGET=_BLANK"
			drupal_add_js("	
				jQuery(document).ready(function () { 
					jQuery('.views-field-field-archivo a').attr('target', '_blank');
				}
			);", "inline");	

	}




/*	MOSTREM ITEMS A LA CISTELLA DE LA COMPRA

    $item_count = 0;
    $items = uc_cart_get_contents();
      if (!empty($items)) foreach ($items as $item) {
            $item_count += $item->qty;
	}
    $vars['cart_reminder'] = $item_count;
*/



/*	COMPROBAMOS QUE EN LA CESTA HAYA ALGUN KIT DE PRODUCTOS
    $item_count = 0;
    $items = uc_cart_get_contents();
    if (!empty($items)) foreach ($items as $item) {
		if ($item->module == "uc_product_kit")
			dpm($item);
	}
*/



}



/* FUNCI�N PARA CAMBIAR EL T�TULO DE LA P�GINA EN LAS P�GINAS DE USUARIO */
function ishopping_form_alter(&$form, $form_state, $form_id) {
  if ($form_id == 'user_register_form') {
    drupal_set_title(t('Create new account'));
  }
  elseif ($form_id == 'user_pass') {
    drupal_set_title(t('Request new password'));
  }
  elseif ($form_id == 'user_login') {
  drupal_set_title(t('Log in'));
  }
}





/**
 * Implements hook_css_alter().
 */
function ishopping_css_alter(&$css) {
  global $theme_key;
  $skin = theme_get_setting('skin');
  if (isset($_COOKIE['nucleus_skin'])) {
    if ($_COOKIE['nucleus_skin'] != $skin) {
      $nucleus_skin = $_COOKIE['nucleus_skin'];
      if (!empty($nucleus_skin) && file_exists(drupal_get_path('theme', $theme_key) . "/skins/" . $nucleus_skin . "/style.css")) {
        $css = drupal_add_css(drupal_get_path('theme', $theme_key) . "/skins/" . $nucleus_skin . "/style.css", array(
          'group' => CSS_THEME,
        ));
      }
      if ($nucleus_skin == "default" || file_exists(drupal_get_path('theme', $theme_key) . "/skins/" . $nucleus_skin . "/style.css")) {
        unset($css[drupal_get_path('theme', $theme_key) . "/skins/" . $skin . "/style.css"]);
      }
    }
  }
}


/* FUNCI�N PARA A�ADIR EL ID DE LA TAXONOM�A A CADA OPCI�N DE MEN�, PARA PODER MOSTRARLA ACTIVA CUANDO ESTAMOS MOSTRANDO UN PRODUCTO */

function ishopping_menu_link(array $variables) {
  $element = $variables['element'];
  $sub_menu = '';
  
  $active = '';
  $titulo = '';

  if ($element['#theme'] == "menu_link__main_menu") {

	$catalog = "catalog/";
	$posicion = strpos($element['#href'],$catalog);

	if (strpos($element['#href'],$catalog) !== false) {
		$inicial = strlen($catalog);
		$ide = substr($element['#href'],$inicial,strlen($element['#href']));
		$active = 'id="' . $ide . '"';
	}
  }


  if ($element['#below']) {
    $sub_menu = drupal_render($element['#below']);
  }
  $output = l($element['#title'], $element['#href'], $element['#localized_options']);
  return '<li' . drupal_attributes($element['#attributes']) . ' ' . $active . '>' . $output . $sub_menu . "</li>\n";
}



/* FUNCI�N PARA ABRIR LOS ARCHIVOS PDF Y DE TEXTO EN UNA NUEVA VENTANA */

function ishopping_file_link($variables) {
  $file = $variables['file'];
  $icon_directory = $variables['icon_directory'];
  $url = file_create_url($file->uri);
  $icon = theme('file_icon', array('file' => $file, 'icon_directory' => $icon_directory));
  // Set options as per anchor format described at
  // http://microformats.org/wiki/file-format-examples
  $options = array(
    'attributes' => array(
      'type' => $file->filemime . '; length=' . $file->filesize,
    ),
  );
  // Use the description as the link text if available.
  if (empty($file->description)) {
    $link_text = $file->filename;
  }
  else {
    $link_text = $file->description;
    $options['attributes']['title'] = check_plain($file->filename);
  }
  //open files of particular mime types in new window
  $new_window_mimetypes = array('application/pdf','text/plain');
  if (in_array($file->filemime, $new_window_mimetypes)) {
    $options['attributes']['target'] = '_blank';
  }
  return '<span class="file">' . $icon . ' ' . l($link_text, $url, $options) . '</span>';
}