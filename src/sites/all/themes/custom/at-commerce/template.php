<?php
// AT commerce
function at_commerce_preprocess(&$vars) {
  if (!array_key_exists('adaptivetheme', list_themes())) {
    drupal_set_message(t('Error! <a href="!link">AT Core</a> base theme not found. Please install and enable Adaptivetheme Core.', array('!link' => 'http://drupal.org/project/adaptivetheme')), 'error', false);
  }
}

/**
 * Override or insert variables into the html template.
 */
function at_commerce_preprocess_html(&$vars) {

  global $theme_key;

  $theme_name = 'at_commerce';
  $path_to_theme = drupal_get_path('theme', $theme_name);


  // Draw stuff
  drupal_add_js($path_to_theme . '/scripts/draw.js');

}
