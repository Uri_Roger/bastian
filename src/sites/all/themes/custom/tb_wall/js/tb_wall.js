var locale = {
  SUBSCRITO: {
    ca: "Ja ets premium",
    es: "Ya eres premium",
    en: "Already premium"
  },
  CANTIDAD: {
    ca: "Cantitat",
    es: "Cantidad",
    en: "Quantity"
  },
  SUBSCRIBE: {
      ca: "Vols saber més? Fes-te de Bastian i gaudeix de tots els continguts.",
      es: "¿Quieres saber mas? Hazte de Bastian y disfruta de todos los contenidos.",
      en: "¿Quieres saber mas? Hazte de Bastian y disfruta de todos los contenidos."
  },
  TRAMITAR: {
      ca: "Tramitar la compra",
      es: "Tramitar Compra",
      en: "Tramitar Compra"
  },
  TARGETA: {
      ca: "Dades de la targeta",
      es: "Datos de la targeta",
      en: "Datos de la targeta"
  }
}
var config = {
  LANGUAGE: 'es',
  THEME_URL: '/sites/all/themes/tb_wall/',
  WINDOW_MEASURES: [],
  RESIZE_THRESHOLD: 20 //miliseconds
};


(function ($) {
  Drupal.TBWall = Drupal.TBWall || {};

// BASTIAN - ANCHO DE BLOQUE EN LA HOME
  Drupal.TBWall.columnWidth = 250;
  Drupal.TBWall.sidebarColumnWidth = 250;
//  Drupal.TBWall.columnWidth = 200;
//  Drupal.TBWall.sidebarColumnWidth = 200;
// BASTIAN - ANCHO DE BLOQUE EN LA HOME

  Drupal.TBWall.toolbarHeight = -1;
  Drupal.TBWall.headerHeight = -1;
  Drupal.TBWall.sidebarIScroll = false;
  Drupal.TBWall.popupIScroll = false;
  Drupal.TBWall.supportedScreens = [0.5, 479.5, 719.5, 959.5, 1049.5, 1235.5, 1585.5, 1890.5];
  Drupal.TBWall.screens = ['empty', 'mobile-vertical', 'mobile', 'tablet-vertical', 'tablet', 'normal-screen', 'wide', 'wide-extra', 'hd'];
  Drupal.TBWall.currentScreen = "";
  Drupal.TBWall.currentWidth = -1;
  Drupal.TBWall.IE8 = navigator.userAgent.search(/MSIE 8.0/) != -1;
  Drupal.TBWall.toolbar = false;
  Drupal.TBWall.masonry_container = false;
  Drupal.TBWall.masonry_sidebar_container = false;

  Drupal.behaviors.actionTBWall = {
    attach: function (context) {
      $(window).scroll(function() {
        Drupal.TBWall.btnToTop();
        Drupal.TBWall.loadActualImages();
      });

      $(window).resize(function(){
        $('body').css({'padding-top': Drupal.TBWall.toolbar ? (Drupal.TBWall.toolbar.height() - (Drupal.TBWall.IE8 ? 10 : 0)) : 0});
        Drupal.TBWall.loadActualImages();
        Drupal.TBWall.updateResponsiveMenu();
        Drupal.TBWall.updateScrollSize();
        Drupal.TBWall.mobilePopup();
        Drupal.TBWall.currentWidth = window.innerWidth ? window.innerWidth : $(window).width();
        Drupal.TBWall.updateMasonryMainWidth(true);
        Drupal.TBWall.updateMasonrySidebarWidth(true);
        Drupal.TBWall.ie8Resize();
        window.setTimeout(function() {
          $('#modalBackdrop').css('width', '100%');
        }, 500);
        var windowWidth = window.innerWidth ? window.innerWidth : $(window).width();
        if(windowWidth > Drupal.TBWall.supportedScreens[4]){
          $('#main-wrapper .page-main-inner, #sidebar-first-wrapper .grid-inner').matchHeights();
        }
      });
    }
  };

  Drupal.TBWall.ie8Resize = function() {
    if(Drupal.TBWall.IE8) {
      current_screen = "";
      for (i = 0; i < Drupal.TBWall.supportedScreens.length; i ++) {
        if (Drupal.TBWall.currentWidth < Drupal.TBWall.supportedScreens[i]) {
          current_screen = Drupal.TBWall.screens[i];
          break;
        }
      }
      if (current_screen != Drupal.TBWall.currentScreen) {
        if (current_screen == 'normal-screen') {
          $style = '<style type="text/css" class="normal-screen-ie8">.sharethis-buttons { position: inherit; bottom: 0px; right: auto; left: 0px; margin-bottom: 10px;}'
          $style += ".gallery-slides, .gallery-thumbs { width: auto !important; }</style>";
          $("#page").append($($style));
        }
        else {
          $("#page style.normal-screen-ie8").remove();
        }
        Drupal.TBWall.currentScreen = current_screen;
      }
    }
  }

  Drupal.TBWall.updateMainColumnWidth = function() {
    Drupal.TBWall.masonry_container.addClass('masonry-reload');
    var container_width = Drupal.TBWall.masonry_container.width();
    var number_column = Math.round(container_width / Drupal.TBWall.columnWidth);
    var column_width = Math.floor(container_width / number_column);
    Drupal.TBWall.masonry_container.find('.grid.tb-wall-single-style').css({
      width: column_width + "px"
    });
    Drupal.TBWall.masonry_container.find('.grid.tb-wall-double-style').css({
      width: (column_width * 2) + "px"
    });
    Drupal.TBWall.masonry_container.find('.grid.tb-wall-triple-style').css({
      width: (column_width * 3) + "px"
    });
    Drupal.TBWall.masonry_container.data('basewidth', column_width);
    return column_width;
  }

  Drupal.TBWall.updateMasonryMainWidth = function(reload) {
    if(Drupal.TBWall.masonry_container) {
      Drupal.TBWall.updateMainColumnWidth();
      if(reload) {
        window.setTimeout(function() {
          Drupal.TBWall.masonry_container.masonry('reload');
        }, 100);
      }
    }
  }

  Drupal.TBWall.updateSidebarColumnWidth = function() {
    Drupal.TBWall.masonry_sidebar_container.addClass('masonry-reload');
    var container_width = Drupal.TBWall.masonry_sidebar_container.width();
    var number_column = Math.round(container_width / Drupal.TBWall.sidebarColumnWidth);
    var column_width = Math.floor(container_width / number_column);
    Drupal.TBWall.masonry_sidebar_container.find('.block, .block.grid-single').css({
      width: column_width + "px"
    });
    Drupal.TBWall.masonry_sidebar_container.find('.block.grid-double').css({
      width: (column_width * 2) + "px"
    });
    Drupal.TBWall.masonry_sidebar_container.find('.block.grid-triple').css({
      width: (column_width * 3) + "px"
    });
    Drupal.TBWall.masonry_sidebar_container.data('basewidth', column_width);
    return column_width;
  }

  Drupal.TBWall.updateMasonrySidebarWidth = function(reload) {
	if(Drupal.TBWall.masonry_sidebar_container) {
	  Drupal.TBWall.updateSidebarColumnWidth();
      if(reload) {
        window.setTimeout(function() {
          Drupal.TBWall.masonry_sidebar_container.masonry('reload');
        }, 20);
      }
	}
  }

  Drupal.TBWall.initMasonry = function() {
    var $container = $('#block-system-main .view .view-content .views-view-grid');
    if($container.length) {
      Drupal.TBWall.masonry_container = $container;
      Drupal.TBWall.masonry_container.addClass('masonry-reload');
      options = {
        itemSelector: '.views-col',
        isResizable: false,
        isAnimated: false,
        columnWidth: function() {
          Drupal.TBWall.masonry_container.removeClass('masonry-reload');
    	  var basewidth = Drupal.TBWall.masonry_container.data('basewidth');
    	  if(basewidth == undefined) {
    		return Drupal.TBWall.updateMainColumnWidth();
    	  }
    	  else {
            return basewidth;
    	  }
        }
      };
      Drupal.TBWall.masonry_container.masonry(options);
      Drupal.TBWall.updateMasonryMainWidth(true);
    }
    var $container = $('#sidebar-first-wrapper .region-sidebar-first');
    if($container.length) {
      Drupal.TBWall.masonry_sidebar_container = $container;
      Drupal.TBWall.masonry_sidebar_container.addClass('masonry-reload');
      options = {
        itemSelector: '.block',
        isResizable: false,
        isAnimated: false,
        columnWidth: function() {
          Drupal.TBWall.masonry_sidebar_container.removeClass('masonry-reload');
          var basewidth = Drupal.TBWall.masonry_sidebar_container.data('basewidth');
          if(basewidth == undefined) {
            return Drupal.TBWall.updateSidebarColumnWidth();
          }
      	  else {
            return basewidth;
      	  }
        }
      }
      Drupal.TBWall.masonry_sidebar_container.masonry(options);
      Drupal.TBWall.updateMasonrySidebarWidth(true);
    }
  };

  Drupal.TBWall.mobilePopup = function() {
    var windowWidth = window.innerWidth ? window.innerWidth : $(window).width();
    if ((Drupal.TBWall.currentWidth == -1) ||
       (windowWidth - Drupal.TBWall.supportedScreens[2]) *
       (Drupal.TBWall.currentWidth - Drupal.TBWall.supportedScreens[2]) < 0) {
      if (windowWidth < Drupal.TBWall.supportedScreens[2]) {
        $('#main-wrapper .colorbox-load.init-colorbox-load-processed.cboxElement').each(function() {
          str = $(this).attr('href');
          parts = str.split("?");
          $(this).removeClass('colorbox-load')
            .removeClass('init-colorbox-load-processed')
            .removeClass('cboxElement')
            .addClass('tb-wall-colorbox-iframe')
            .attr('href', parts[0]);
        });
      }
      else {
    	$('#main-wrapper .tb-wall-colorbox-iframe').each(function() {
          $(this).addClass('colorbox-load')
          .addClass('init-colorbox-load-processed')
          .addClass('cboxElement')
          .removeClass('tb-wall-colorbox-iframe')
          .attr('href', $(this).attr('href') + "?tb_wall_iframe=1&width=550&height=600&iframe=true");
    	})
      }
    }
  }

  Drupal.TBWall.iScrollPopupInit = function() {
    el = $('body.tb-wall-popup-iframe #block-system-main .block-inner');
    if(el.length && !Drupal.TBWall.IE8) {
        el.css({height: $(window).height()});
        Drupal.TBWall.popupIScroll = new iScroll(el[0], {vScroll: true, hScroll: false, vScrollbar: true, hScrollbar: false, scrollbarClass: 'myScrollbar', useTransform: false});
    }
  }

  Drupal.TBWall.iScrollInit = function() {
    if($("#menu-left-inner").length) {
      Drupal.TBWall.sidebarIScroll = new iScroll('menu-left-inner', {
        vScroll: true,
        hScroll: false,
        vScrollbar: true,
        hScrollbar: false,
        scrollbarClass: 'myScrollbar',
        useTransform: false
      });
    }
  }

  Drupal.TBWall.initLazyload = function() {
    $('img[data-src]').each(function() {
      if(!$(this).hasClass('loading-icon')) {
        $(this).addClass('loading-icon').parent().append('<img class="lazyloader-icon" style="position: absolute;" src="' + Drupal.TBWall.lazyload_icon + '"/>');
      }
    });
  }

  Drupal.TBWall.loadActualImages = function(){
    if(Drupal.TBWall.lazyloadFinished) {
      images = $('img[data-src]');
      images.each(function(){
        if (Drupal.TBWall.windowView(this) && $(this).attr('data-src')){
          Drupal.TBWall.loadImage(this);
          $(this).fadeIn('slow');
        }
      });
    }
  };

  Drupal.TBWall.windowView = function(image){
    var windowHeight = $(window).height(),
    windowWidth  = $(window).width(),
    windowBottom = windowHeight + $(window).scrollTop(),
    imageTop     = $(image).offset().top;
    return windowBottom >= imageTop;
  };

  Drupal.TBWall.loadImage = function(image){
    var img = document.createElement('img');
    var src = $(image).attr('data-src');
    $(image).removeAttr('data-src')
    $(img).attr('src', src);
    $(img).imagesLoaded(function(){
      $(image).attr('src', src);
      $(image).removeClass('loading-icon');
      $(image).parent().find('img.lazyloader-icon').remove();
    });
  };

  Drupal.TBWall.updateScrollSize = function() {
    var left_menu = $('#menu-left-wrapper');
    if (left_menu.length) {
      var window_height = window.innerHeight ? window.innerHeight : $(window).height();
      var top = left_menu.offset().top;
      left_menu.css({
        'height': window_height - top
      });
      if(Drupal.TBWall.sidebarIScroll && !Drupal.TBWall.IE8) {
        Drupal.TBWall.sidebarIScroll.refresh();
        Drupal.TBWall.sidebarIScroll.scrollTo(0, 0, 300);
      }
      else if(Drupal.TBWall.IE8){
        $('#menu-left-inner').mCustomScrollbar("update");
        $('#menu-left-inner').mCustomScrollbar("scrollTo", 0);
      }
    }
  }

  Drupal.TBWall.eventStopPropagation = function(event) {
    if (event.stopPropagation) {
      event.stopPropagation();
    }
    else if (window.event) {
      window.event.cancelBubble = true;
    }
  }

  Drupal.TBWall.updateResponsiveMenu = function(){
    var windowWidth = window.innerWidth ? window.innerWidth : $(window).width();
    if ((Drupal.TBWall.currentWidth == -1) ||
       (windowWidth - Drupal.TBWall.supportedScreens[4]) *
       (Drupal.TBWall.currentWidth - Drupal.TBWall.supportedScreens[4]) < 0) {
      if(windowWidth < Drupal.TBWall.supportedScreens[4]){
        $('#menu-bar-wrapper').css({display: 'none'});
        $('#menu-left-wrapper').css({display: 'none'});
        $('#header .responsive-menu-button').show();
      }
      else{
/*        $('#header .responsive-menu-button').hide(); */
        $('#menu-bar-wrapper').css({display: ''});
        $('#menu-left-wrapper').css({display: ''});
      }
    }
  }

  Drupal.TBWall.btnToTop = function(){
    if($(window).scrollTop()) {
      $('#button-btt').fadeIn(1000);
    }
    else {
      $('#button-btt').fadeOut(1000);
    }
  }

  Drupal.TBWall.initResponsiveMenu = function() {
      Drupal.TBWall.updateResponsiveMenu();
      $('#header .tb-left-menu-button').click(function(e){
        var target = $('#menu-left-wrapper');
        target.toggleClass('open');
        target.css({display: target.css('display') != 'none' ? 'none' : 'block'});
        $('#menu-bar-wrapper').css({display: 'none'});
        $('#header-wrapper').css({position: 'fixed'});
        Drupal.TBWall.eventStopPropagation(e);
        Drupal.TBWall.updateScrollSize();
      });
      $('#header .tb-main-menu-button').click(function(e){
        var target = $('#menu-bar-wrapper');
        $('#menu-left-wrapper').css({display: 'none'});
        if(target.css('display') == 'none') {
          window.scrollTo(0, 0);
          target.css({display: 'block'});
          $('#header-wrapper').css({position: 'relative'});
        }
        else {
          target.css({display: 'none'});
          $('#header-wrapper').css({position: 'fixed'});
        }
        Drupal.TBWall.eventStopPropagation(e);
      });
  };
  Drupal.TBWall.initScroll = function() {
    $("#menu-left-inner").mCustomScrollbar({
        set_width:false,
        set_height: '100%',
        horizontalScroll:false,
        scrollInertia:550,
        scrollEasing:"easeOutCirc",
        mouseWheel:"auto",
        autoDraggerLength:true,
        scrollButtons:{
          enable:false,
          scrollType:"continuous",
          scrollSpeed:20,
          scrollAmount:40
        },
        advanced:{
          updateOnBrowserResize:true,
          updateOnContentResize:false,
          autoExpandHorizontalScroll:false
        },
        callbacks:{
          onScroll:function(){},
          onTotalScroll:function(){},
          onTotalScrollOffset:0
        }
      });
  };
  function randomIntFromInterval(min,max)
{
    return
}
  Drupal.TBWall.addEscaparateItems = function(items_selector) {
      var url = "/escaparate?oasync=1";
      if (config.LANGUAGE !== "es") {
          url = "/"+config.LANGUAGE+"/escaparate?oasync=1";
      }
      $.ajax({
          url: url
        })
      .done(function( data ) {

        $('.field-name-field-sello-calidad-auto .field-items .field-item').each(function() {
            if ($(this).is(':empty')) {
              $(this).parent().parent().remove();
            }
        });

        $(data).find('.grid.views-col').each(function(){
          var step = 0;
          if ($(items_selector).length > 20) {
              step = $(items_selector).length - 20;
          }
          var pos = Math.floor(Math.random()*($(items_selector).length-step)+step);   //Math.floor(Math.random() * $(items_selector).length) + step;
          console.log("loading in pos: "+pos+"  maximum of: "+$(items_selector).length);
          $('.views-view-grid > .grid.views-col:eq('+pos+')').after(this);
        });
        if(Drupal.TBWall.masonry_container) {
          Drupal.TBWall.masonry_container.masonry('reload');
        }
        $(items_selector).imagesLoaded(function() {
          if(Drupal.TBWall.masonry_container) {
            Drupal.TBWall.updateMasonryMainWidth();
            Drupal.TBWall.masonry_container.masonry( 'reload', function() {
              Drupal.TBWall.lazyloadFinished = false;
              Drupal.TBWall.initLazyload();
              window.setTimeout(function() {
                Drupal.TBWall.lazyloadFinished = true;
                Drupal.TBWall.loadActualImages();
              }, 100);


            });
          }
        });
      });
  }

  $(document).ready(function(){
    config.LANGUAGE = $('html').attr('lang');
    $.each($('.menuparent.nolink'), function(){
        if ($(this).text() == 'Anónimo') {
            $(this).text('Log in');
        }
    });
    $('.field-name-field-newsletter .form-item').prepend('<label>Newsletter</label>');
   $.each($('.language-switcher-locale-url a'), function(el){
       if (location.search !== "")
        $(this).attr('href', $(this).attr('href')+location.search);
   });

    $('.field-name-field-sello-calidad-auto .field-items .field-item').each(function() {
      if ($(this).is(':empty')) {
        $(this).parent().parent().remove();
      }
    });
    
    $("button[data-action='subscribe-block']").click(function(e){
        e.preventDefault();
        var nid = $("ul.prices > li").attr('data-product');
        window.location = "/cart/add/p"+nid+"?destination=cart";
    });
    $("li[data-subs]").click(function(e){
        e.preventDefault();
        $("ul > li[data-subs], ul > li[data-tab]").removeClass('active');
        $("ul.prices > li[data-tab='"+$(this).attr('data-subs')+"']").addClass("active");
        $(this).addClass('active');
        
    });
    
    if (!$("#page").hasClass('premium')) {
        $("#node_product_full_group_inferior > .group-texto").addClass('trimmed');
        $("#node_product_full_group_inferior > .group-texto").append('<div class="abs subscribe-link"><a href="'+$("#block-block-16 .block-content > a").attr('href')+'">'+locale.SUBSCRIBE[config.LANGUAGE]+"</a></div>");
    }
    
    if ($('.article.recurso').length > 0 && $(window).width() > 550) {
        var elem3 = $('.group-superior .group-product-right').offset().top+$('.group-superior .group-product-right').height();
        var elem4 = $('.group-inferior > div').eq(1).offset().top;
        if (elem3-elem4+15 < 0) {
            //$('.group-inferior > div').eq(1).css('margin-top', elem3-elem4+15);
            //if ($('.group-comprar').length > 0)
              //  $('.group-inferior > div').eq(2).css('margin-top', elem3-elem4+15);
        }  
    }
    $('label[for="edit-panes-uc-termsofservice-agreement-checkout-tos-agree-agreed"]').html($('label[for="edit-panes-uc-termsofservice-agreement-checkout-tos-agree-agreed"]').text().replace('anteriores', '<a target="_blank" href="/condiciones-generales-y-politica-de-privacidad">planteados aquí</a>'));
    $('label[for="edit-panes-uc-termsofservice-agreement-checkout-tos-agree-agreed"]').html($('label[for="edit-panes-uc-termsofservice-agreement-checkout-tos-agree-agreed"]').text().replace('anteriors', '<a target="_blank" href="/ca/condicions-generals-i-politica-de-privacitat">plantejats aqui</a>'));
    $('#payment-pane .description').html('<h2>'+locale.TRAMITAR[config.LANGUAGE]+'</h2>');
        $('#payment-details').prepend('<h2>'+locale.TARGETA[config.LANGUAGE]+'*</h2>');
    $('#payment-pane .description').append($('#line-items-div, .form-item-panes-payment-payment-method'));
     $(document).ajaxStop(function() {
         $('label[for="edit-panes-uc-termsofservice-agreement-checkout-tos-agree-agreed"]').html($('label[for="edit-panes-uc-termsofservice-agreement-checkout-tos-agree-agreed"]').text().replace('anteriores', '<a target="_blank" href="/condiciones-generales-y-politica-de-privacidad">planteados aquí</a>'));
        $('#payment-pane .description').html('<h2>'+locale.TRAMITAR[config.LANGUAGE]+'</h2>');
        $('#payment-details').prepend('<h2>'+locale.TARGETA[config.LANGUAGE]+'*</h2>');
        $('#payment-pane .description').append($('#line-items-div, .form-item-panes-payment-payment-method'));
     });
    var view_selector    = 'div.view-front-page-2.view-display-id-page';
    var items_selector   = view_selector + ' .grid.views-col';
    if ($(view_selector).length > 0) {
        if (!$('#page').hasClass('premium')) {
            Drupal.TBWall.addEscaparateItems(items_selector);
        }
        var querystring = window.location.search.substring(1);
        if (querystring !== "") {
            $('.field-name-field-imagen-pequena a, .field-name-field-titulo-link a').each(function(){
                var href = $(this).attr('href');
                if(href) {
                    href += (href.match(/\?/) ? '&' : '?') + querystring;
                    $(this).attr('href', href);
                }
            });
        }
    }

    $('table.sticky-table thead tr th:first-child').remove();

    $('table.sticky-table tbody tr').each(function(index) {
      rem = $(this).find('td.remove').html();
      $(this).find('td.desc').append(rem);
      $(this).find('td.remove').remove();
    });

    $('#uc-cart-view-form .form-actions .button.edit-empty, #uc-cart-view-form .form-actions .button.edit-update').remove();
    $('table.sticky-table thead tr th abbr').empty().append(locale.CANTIDAD[config.LANGUAGE]);
    $('table.sticky-table thead tr th abbr').css('margin-left', '50px');
    $('table.sticky-table thead tr th abbr').css('padding-right', '50px');
    $('table.sticky-table thead tr th:last-child').css('text-align', 'right');

    $('table.sticky-table .subtotal').removeAttr('colspan');
    $('table.sticky-table .subtotal').before('<td colspan="3"></td>')


    $('.views-exposed-form ul.bef-tree li .form-item span').remove();
    $('.views-exposed-form ul.bef-tree li .form-item').each(function(index) {
      $(this).append('<span class="ui-icon ui-icon-triangle-1-e"></span>');
    });
    if (!$("#page").hasClass("premium")) {
        $('.views-exposed-form #edit-ambito-wrapper .bef-tree-depth-0 > .ui-accordion > ul.bef-tree-child').remove();
    }
    $.each($('.views-exposed-form .bef-tree-depth-0 > .ui-accordion ul.bef-tree-child > li'), function(el) {
        var active = true;
        $.each($(this).find('input[type="checkbox"]'), function(i){
            if (!$(this).attr('checked')) {
                console.log($(this).attr('checked'));
                active = false;
            }
        })
        if (active) {
            $(this).parent().siblings(".form-type-bef-checkbox").addClass('active');
        }
    });
    $('.views-exposed-form .bef-tree-depth-0 > .ui-accordion > .form-type-bef-checkbox label').click(function(e) {
      if (!$("#page").hasClass("premium") && $(this).closest("#edit-ambito-wrapper").length > 0) {
          e.preventDefault();
          window.location = "node/172";
          return;
      }
      if (! $(this).parent().hasClass('active')) {
        $(this).parent().addClass('active');
        $.each($(this).parent().siblings('ul.bef-tree-child').find('input[type="checkbox"]'), function(input) {
            $(this).attr('checked', true);    
        }); 
      }
      else {
        $(this).parent().removeClass('active');
        $.each($(this).parent().siblings('ul.bef-tree-child').find('input[type="checkbox"]'), function(input) {
            $(this).attr('checked', false);    
        });
      }
      $(this).closest('form').submit();
      e.preventDefault();
    });

  });
  $(window).load(function() {

      /*if ($('.article.node-full').length > 0) {
         var galeria = $('#node_product_full_group_product_left')
           , info = $('#node_product_full_group_product_right');
           if (galeria.height() > info.height()) {
               info.find('.group-detalles').css('height', galeria.height() - $('#block-block-16').height());
           } else {
               $('.group-galleryformatter').css('height', info.height() - $('.group-compartir').height());
           }
           $('.group-comprar').css('height', $('#block-block-18').height());
      }*/
     if ($('.article.recurso').length > 0 && $(window).width() > 550) {
        var elem1 = $('.group-product-left').offset().top+$('.group-product-left').height();
        var elem2 = $('.group-texto').offset().top;
        if (elem1-elem2+20 < 0)
            $('.group-texto').css('margin-top', elem1-elem2+20);

        var elem2_1 = $('.group-texto').offset().top+$('.group-texto').height();
        
        if ($('#block-commentsblock-comment-form-block').length > 0 && elem2_1-$('#block-commentsblock-comment-form-block').offset().top+40 < 0)
            $('#block-commentsblock-comment-form-block').css('margin-top', elem2_1-$('#block-commentsblock-comment-form-block').offset().top+40);
        
    }

      $('.faq-question').click(function() {
        if ($(this).prev().hasClass('ui-icon-triangle-1-e')) {
          $(this).prev().removeClass('ui-icon-triangle-1-e').addClass('ui-icon-triangle-1-s');
        }
        else {
          $(this).prev().removeClass('ui-icon-triangle-1-s').addClass('ui-icon-triangle-1-e');
        }
      });

      if ($('.article').attr('about') === '/product/el-club-de-bastian') {
        if (! $('#edit-ambito-wrapper').hasClass('non-premium')) {
          $('.product-info .uc-price').empty().append(locale.SUBSCRITO[config.LANGUAGE]);
          $('.add-to-cart').empty();
        }
      }



        Drupal.TBWall.mobilePopup();
        Drupal.TBWall.currentWidth = window.innerWidth ? window.innerWidth : $(window).width();
        Drupal.TBWall.toolbar = $('#toolbar').length ? $("#toolbar") : false;
        Drupal.TBWall.btnToTop();
        $('#button-btt').smoothScroll();
        Drupal.TBWall.initResponsiveMenu();
        if (Drupal.TBWall.IE8) {
          Drupal.TBWall.initScroll();
          Drupal.TBWall.updateScrollSize();
        } else {
          Drupal.TBWall.iScrollInit();
          Drupal.TBWall.updateScrollSize();
        }
        Drupal.TBWall.iScrollPopupInit();
        Drupal.TBWall.initMasonry();
        $("#block-system-main .view .view-content .views-view-grid").imagesLoaded(function() {
          Drupal.TBWall.initLazyload();
          if(Drupal.TBWall.masonry_container) {
            Drupal.TBWall.masonry_container.masonry('reload');
          }
          window.setTimeout(function() {
            Drupal.TBWall.lazyloadFinished = true
            Drupal.TBWall.loadActualImages();
          }, 100);
        });
        $("body.tb-wall-popup-iframe").find("a").attr('target', '_parent');
        Drupal.TBWall.ie8Resize();
  });
})(jQuery);


function ambito_taxonomy_name(ambito) {
	switch(ambito) {
		case "204","609": nombre = "personal"; break;
		case "205","613": nombre = "psicomotriz"; break;
		case "206","615": nombre = "emocional"; break;
		case "209","618": nombre = "ambiental"; break;
		case "211","621": nombre = "intelectual"; break;
		case "207","623": nombre = "interpersonal"; break;
		case "208","628": nombre = "socialcivico"; break;
		case "210","631": nombre = "trascendente"; break;
		default:	nombre = "personal";
	}
	return nombre;
}

