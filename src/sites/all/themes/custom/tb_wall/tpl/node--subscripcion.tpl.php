<div id="article-<?php print $node->nid; ?>" class="article node-subs <?php print $classes; ?> clearfix"<?php print $attributes; ?>>
    <div id="node_product_full_group_superior" class="group-superior field-group-div">
        <div id="node_product_full_group_product_left" class="group-product-left field-group-div">
            <div class="subscription">
                <div class="subs-info">
                <h2 class="title"><?php print t("¡SELECCIONA TU PLAN!"); ?></h2>
                <p class="subtitle"><?php print t("Accede a todos los <strong>contenidos Bastian</strong> sin restricciones. Con tu subscripción podrás acceder a todo el contenido que hemos preparado para tí para que disfrutes con sentido de todo el catálogo de libros, actividades, manualidades y un sin fin de recursos con tus hijos. Además, disfrutarás de descuentos en libros y otros recursos. Escoge el plan que mejor se adapta a ti"); ?>:</p>
                <?php $block = block_load('bastian', 'subscription');
                print drupal_render(_block_get_renderable_array(_block_render_blocks(array($block)))); ?>
                <ul class="subs-settings">
                    <li>
                        <span class="icon glasses"></span>
                        <p><?php print t("Todos los recursos 100% especializados"); ?></p>
                    </li>
                    <li>
                        <span class="icon book"></span>
                        <p><?php print t("Material exclusivo para los tuyos"); ?></p>
                    </li>
                    <li>
                        <span class="icon face"></span>
                        <p><?php print t("Disfrutar tranquilo con tu familia"); ?></p>
                    </li>
                </ul>
                </div>
            </div>
         </div>
         <div id="node_product_full_group_product_right" class="group-product-right field-group-div">
            <?php //print render($content['group_superior']['group_product_right']['group_add_to_cart']); ?>
            <img src="<?php print file_create_url("public://animacio3.gif"); ?>" alt="Subscríbete">
         </div>
    </div>


  <?php
   if ($links = render($content['links'])): ?>
    <div class="menu node-links clearfix"><?php //print $links; ?></div>
  <?php endif; ?>

  <?php print render($content['comments']); ?>
</div>
