<div id="article-<?php print $node->nid; ?>" class="article recurso <?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  <?php if ($tb_wall_first_field): ?>
    <div class="first-field">
    <?php print($tb_wall_first_field); ?>
    <?php print render($content['group_contenido']['group_franja_4']); ?>
    </div>
  <?php endif; ?>

  <div<?php print $content_attributes; ?>>
    <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
      global $user;
//      dpm($content);

      
      ?>
      <div id="node_product_full_group_superior" class="group-superior field-group-div">
        <?php print render($content['group_superior']['group_product_left']); ?>
        <div id="node_product_full_group_product_right" class="group-product-right field-group-div">
            <?php if (!in_array('Premium',$user->roles)): ?>
            <?php print render(_block_get_renderable_array(_block_render_blocks(array(block_load('block', 16)))));  //block per usuaris no premium ?>
            <?php endif; ?>
            <div class="group-detalles">
            <?php // SI NO ES PREMIUM, NO MOSTRAMOS LOS �MBITOS
              hide($content['group_superior']['group_product_right']['group_detalles']['group_caracteristicas']['group_ambito']);
              if (!in_array('Premium',$user->roles)): ?>
                  <?php print render($content['group_superior']['group_product_right']['group_detalles']['group_caracteristicas']); ?>
                  <div class="group-ambito">
                    <h3><?php print t('¿Qué aprendes con "').$title.'"?'; ?></h3>
                    <div class="content">
                        <?php print render(_block_get_renderable_array(_block_render_blocks(array(block_load('block', 17)))));  //block per usuaris no premium ?>
                    </div>
                  </div>
              <?php else: ?>
                <?php print render($content['group_superior']['group_product_right']['group_detalles']['group_caracteristicas']); ?>
                  <div class="group-ambito">
                    <h3><?php print t('¿Qué aprendes con "').$title.'"?'; ?></h3>
                    <div class="content">
                        <?php print render($content['group_superior']['group_product_right']['group_detalles']['group_caracteristicas']['group_ambito']);  //block per usuaris no premium ?>
                    </div>
                  </div>
              <?php endif; ?>
            </div>
            <?php if (in_array('Premium',$user->roles)): ?>
               <?php print render($content['group_superior']['group_product_right']['group_tags']); ?>
            <?php endif; ?>
        </div>
      </div>
      <div id="node_product_full_group_inferior" class="group-inferior field-group-div">
      <?php print render($content['group_inferior']['group_texto']); ?>
      <?php print render($content['group_inferior']['group_add_to_cart']); ?>
      <?php print render(_block_get_renderable_array(_block_render_blocks(array(block_load('block', 18))))); ?>
      <?php if (!in_array('Premium',$user->roles)): ?>
        <?php print render($content['group_superior']['group_product_right']['group_tags']); ?>
      <?php endif; ?>
      </div>
  </div>

  <?php
   if ($links = render($content['links'])): ?>
    <div class="menu node-links clearfix"><?php //print $links; ?></div>
  <?php endif; ?>

  <?php print render($content['comments']); ?>
</div>
